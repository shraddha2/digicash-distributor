package com.digicash.distributor.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.digicash.distributor.Activity.OfflineRechargeActivity;
import com.digicash.distributor.Model.Item;
import com.digicash.distributor.R;

import java.util.ArrayList;

public class OperatorListAdapter extends RecyclerView.Adapter<OperatorListAdapter.ViewHolder> implements Filterable {
    private ArrayList<Item> mArrayList;
    private ArrayList<Item> mFilteredList;
    Context ctx;
    int rech_type;

    public OperatorListAdapter(ArrayList<Item> arrayList, Context ctx, int rech_type) {
        mArrayList = arrayList;
        mFilteredList = arrayList;
        this.ctx=ctx;
        this.rech_type=rech_type;

    }

    @Override
    public OperatorListAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.operator_list_row, viewGroup, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(OperatorListAdapter.ViewHolder viewHolder, int i) {

        viewHolder.txt_operator_name.setText(mFilteredList.get(i).getTxt());
        viewHolder.txt_id.setText(mFilteredList.get(i).getTxt_id());
        viewHolder.img_operator_logo.setImageResource(mFilteredList.get(i).getImageId());

    }

    @Override
    public int getItemCount() {
        return mFilteredList.size();
    }

    @Override
    public Filter getFilter() {

        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {

                String charString = charSequence.toString();

                if (charString.isEmpty()) {

                    mFilteredList = mArrayList;
                } else {

                    ArrayList<Item> filteredList = new ArrayList<>();

                    for (Item androidVersion : mArrayList) {

                        if (androidVersion.getTxt().toLowerCase().contains(charString)) {

                            filteredList.add(androidVersion);
                        }
                    }

                    mFilteredList = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = mFilteredList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                mFilteredList = (ArrayList<Item>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView txt_operator_name,txt_id;
        private ImageView img_operator_logo;
        public ViewHolder(View view) {
            super(view);

            txt_operator_name = (TextView)view.findViewById(R.id.txt_operator_name);
            txt_id = (TextView)view.findViewById(R.id.txt_id);
            img_operator_logo = (ImageView)view.findViewById(R.id.img_operator_logo);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    String operator_name=txt_operator_name.getText().toString();
                    String id=txt_id.getText().toString();
                    Intent intent=new Intent(ctx, OfflineRechargeActivity.class);
                    intent.putExtra("operator_name",operator_name);
                    intent.putExtra("operator_id",id);
                    intent.putExtra("rech_type",rech_type);
                    ((Activity) ctx).startActivity(intent);
                    ((Activity) ctx).finish();//finishing activity

                }
            });

        }
    }

}