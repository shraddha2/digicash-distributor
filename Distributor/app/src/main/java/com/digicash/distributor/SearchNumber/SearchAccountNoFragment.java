package com.digicash.distributor.SearchNumber;

import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.digicash.distributor.DefineData;
import com.digicash.distributor.DijiCashDistributor;
import com.digicash.distributor.Model.DividerItemDecoration;
import com.digicash.distributor.Model.HTTPURLConnection;
import com.digicash.distributor.Model.Item;
import com.digicash.distributor.R;
import com.digicash.distributor.Receiver.ConnectivityReceiver;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class SearchAccountNoFragment extends Fragment implements ConnectivityReceiver.ConnectivityReceiverListener {
    private List<Item> movieList = new ArrayList<>();
    private RecyclerView recyclerView;
    private SearchAccountAdapter mAdapter;
    SharedPreferences sharedpreferences;
    String token,search_num;
    ConstraintLayout progress_linear,linear_container,rel_img_bg,rel_no_records,rel_no_internet;
    TextView txt_title,txt_label1,txt_err_msgg,txt_network_msg;
    EditText edt_search_number;
    ImageView img_search;

    //Typeface font ;
    public SearchAccountNoFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView= inflater.inflate(R.layout.fragment_search_account_no, container, false);
        String mess = getResources().getString(R.string.app_name);
        getActivity().setTitle(mess);
        //setHasOptionsMenu(true);

        edt_search_number=(EditText)rootView.findViewById(R.id.edt_search_number);
        img_search=(ImageView)rootView.findViewById(R.id.img_search);

        sharedpreferences = getActivity().getSharedPreferences(DefineData.LOGIN_MINKSPAY_PREFERENCE, DijiCashDistributor.getInstance().MODE_PRIVATE);
        token=sharedpreferences.getString(DefineData.TOKEN_KEY,"");

        recyclerView = (RecyclerView) rootView.findViewById(R.id.rc_recharge_history);
        txt_label1 = (TextView) rootView.findViewById(R.id.txt_label1);
        txt_title= (TextView) rootView.findViewById(R.id.txt_title);
        txt_err_msgg= (TextView) rootView.findViewById(R.id.txt_err_msgg);
        txt_network_msg= (TextView) rootView.findViewById(R.id.txt_network_msg);

        progress_linear= (ConstraintLayout) rootView.findViewById(R.id.loading);
        linear_container= (ConstraintLayout) rootView.findViewById(R.id.container);
        rel_img_bg= (ConstraintLayout) rootView.findViewById(R.id.rel_img_bg);
        rel_no_records= (ConstraintLayout) rootView.findViewById(R.id.rel_no_records);
        rel_no_internet= (ConstraintLayout) rootView.findViewById(R.id.rel_no_internet);

        txt_title.setText("Search Account Number");

        linear_container.setVisibility(View.GONE);
        rel_no_records.setVisibility(View.GONE);
        rel_img_bg.setVisibility(View.VISIBLE);
        progress_linear.setVisibility(View.GONE);
        rel_no_internet.setVisibility(View.GONE);

        mAdapter = new SearchAccountAdapter(movieList,getActivity(),"Search Account Number");
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity()));
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

        img_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String query=edt_search_number.getText().toString();
                boolean isError=false;

                if(null==query||query.length()==0)
                {
                    isError=true;
                    edt_search_number.setError("Invalid Account Number");
                }

                if(!isError) {
                    search_num=query;

                    if(checkConnection()) {
                        linear_container.setVisibility(View.GONE);
                        rel_no_records.setVisibility(View.GONE);
                        rel_img_bg.setVisibility(View.VISIBLE);
                        progress_linear.setVisibility(View.GONE);
                        rel_no_internet.setVisibility(View.GONE);
                        new FetchDetails().execute();
                    }else{
                        linear_container.setVisibility(View.GONE);
                        rel_no_records.setVisibility(View.GONE);
                        rel_img_bg.setVisibility(View.GONE);
                        progress_linear.setVisibility(View.GONE);
                        rel_no_internet.setVisibility(View.VISIBLE);
                    }
                }
            }
        });

        return rootView;
    }

    private class FetchDetails extends AsyncTask<Void, Void, Void> {

        JSONObject response;
        @Override
        protected void onPreExecute() {
            movieList.clear();
            linear_container.setVisibility(View.GONE);
            rel_no_records.setVisibility(View.GONE);
            rel_img_bg.setVisibility(View.GONE);
            progress_linear.setVisibility(View.VISIBLE);

        }
        @Override
        protected Void doInBackground(Void... params) {
            HTTPURLConnection service = new HTTPURLConnection();
            try{
                HashMap<String, String> parameters = new HashMap<String, String>();
                parameters.put("phoneNo", search_num);
                this.response = new JSONObject(service.POST(DefineData.SEARCH_ACCOUNT_NUMBER,parameters,token));
            }catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            if(response!=null) {
                try {
                    if (response.getBoolean("error")) {
                        String msg="Error";
                        if(response.has("data")) {
                            msg = response.getString("data");
                        }
                        txt_err_msgg.setText(msg+"");
                        linear_container.setVisibility(View.GONE);
                        rel_no_records.setVisibility(View.VISIBLE);
                        rel_img_bg.setVisibility(View.GONE);
                        progress_linear.setVisibility(View.GONE);
                    } else {

                        JSONArray jsonArray = response.getJSONArray("data");
                        if(jsonArray.length()!=0) {
                            for (int i = 0; i < jsonArray.length(); i++) {
                                Item superHero = null;
                                JSONObject json2 = null;
                                try {
                                    //Getting json
                                    json2 = jsonArray.getJSONObject(i);
                                    String Id = json2.getString("id");
                                    String Amount = json2.getString("amount");
                                    String Surcharge = json2.getString("surcharge");
                                    String Time = json2.getString("time");
                                    String Status = json2.getString("status");
                                    String Sender = json2.getString("sender");
                                    String BeneficiaryName = json2.getString("beneficiaryName");
                                    String RecipientAccount = json2.getString("recipientAccount");
                                    String RecipientMobile = json2.getString("recipientMobile");
                                    String RecipientBank = json2.getString("recipientBank");
                                    String BankRefNo = json2.getString("bankRefNo");
                                    superHero = new Item(Id,Amount,"Sender: "+Sender,"Beneficiary: "+BeneficiaryName,
                                            "Surcharge: "+Surcharge,"Recipient Mobile: "+RecipientMobile,Time,
                                            Status,"Recipient Bank: "+RecipientBank,"Recipient Account: "+RecipientAccount,
                                            "Bank Ref.No.: "+BankRefNo);

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    txt_err_msgg.setText("Error in parsing");
                                    linear_container.setVisibility(View.GONE);
                                    rel_no_records.setVisibility(View.VISIBLE);
                                    rel_img_bg.setVisibility(View.GONE);
                                    progress_linear.setVisibility(View.GONE);
                                }
                                //Adding the superhero object to the list
                                movieList.add(superHero);
                                mAdapter.notifyDataSetChanged();
                                linear_container.setVisibility(View.VISIBLE);
                                rel_no_records.setVisibility(View.GONE);
                                rel_img_bg.setVisibility(View.GONE);
                                progress_linear.setVisibility(View.GONE);
                            }
                        }else{
                            txt_err_msgg.setText("Records Not Found");
                            linear_container.setVisibility(View.GONE);
                            rel_no_records.setVisibility(View.VISIBLE);
                            rel_img_bg.setVisibility(View.GONE);
                            progress_linear.setVisibility(View.GONE);
                        }

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    txt_err_msgg.setText("Error in parsing");
                    linear_container.setVisibility(View.GONE);
                    rel_no_records.setVisibility(View.VISIBLE);
                    rel_img_bg.setVisibility(View.GONE);
                    progress_linear.setVisibility(View.GONE);
                }
            }else{
                txt_err_msgg.setText("Empty server response");
                linear_container.setVisibility(View.GONE);
                rel_no_records.setVisibility(View.VISIBLE);
                rel_img_bg.setVisibility(View.GONE);
                progress_linear.setVisibility(View.GONE);
            }

        }
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {

    }

    @Override
    public void onResume() {

        super.onResume();
        edt_search_number.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    edt_search_number.clearFocus();
                    getView().requestFocus();
                }
                return false;
            }
        });
        DijiCashDistributor.getInstance().setConnectivityListener(this);
        getView().setFocusableInTouchMode(true);

    }

    // Method to manually check connection status
    private boolean checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        return isConnected;
    }
}
