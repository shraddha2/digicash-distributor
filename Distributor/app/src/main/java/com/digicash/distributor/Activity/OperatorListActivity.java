package com.digicash.distributor.Activity;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.digicash.distributor.Adapter.OperatorListAdapter;
import com.digicash.distributor.Model.Item;
import com.digicash.distributor.R;

import java.util.ArrayList;

/**
 * Created by shraddha on 06-04-2018.
 */

public class OperatorListActivity extends AppCompatActivity {
    private ArrayList<Item> movieList = new ArrayList<>();
    private RecyclerView recyclerView;
    private OperatorListAdapter mAdapter;
    TextView txt_error;
    int type=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_operator_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        setTitle("Select Operator");
        type=getIntent().getIntExtra("type",0);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        txt_error= (TextView) findViewById(R.id.txt_error);
        recyclerView = (RecyclerView) findViewById(R.id.rc_retailers);
        movieList.clear();
        if(type==1) {
            movieList = fill_with_data1();
        }
        if(type==2) {

            movieList = fill_with_data2();
        }
        if(type==3) {

            movieList = fill_with_data3();
        }

        mAdapter = new OperatorListAdapter(movieList,this,type);
        recyclerView.setLayoutManager(new GridLayoutManager(getApplicationContext(), 3));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

    }

    public ArrayList<Item> fill_with_data1() {

        ArrayList<Item> data = new ArrayList<>();
        data.add(new Item( R.drawable.ic_airtel, "Airtel","A"));
        data.add(new Item( R.drawable.ic_aircel, "Aircel","AIR"));
        data.add(new Item( R.drawable.ic_idea_logo, "Idea","I"));
        data.add(new Item( R.drawable.ic_vodafone, "Vodafone","V"));
        data.add(new Item( R.drawable.ic_bsnl, "Bsnl-Topup","BT"));
        data.add(new Item( R.drawable.ic_bsnl, "Bsnl-STV","BS"));
        data.add(new Item( R.drawable.ic_telenor, "Telenor-Topup","U"));
        data.add(new Item( R.drawable.ic_telenor, "Telenor-Special","US"));
        data.add(new Item( R.drawable.ic_jio, "Jio","RJ"));
        data.add(new Item( R.drawable.ic_docomo, "Docomo-Topup","D"));
        data.add(new Item( R.drawable.ic_docomo, "Docomo-Special","DS"));
        data.add(new Item( R.drawable.ic_mtnl, "Mtnl-Topup","MTT"));
        data.add(new Item( R.drawable.ic_mtnl, "Mtnl-Special","MTR"));
        return data;
    }

    public ArrayList<Item> fill_with_data2() {

        ArrayList<Item> data = new ArrayList<>();
        data.add(new Item( R.drawable.ic_airtel, "Airtel-Postpaid","AP"));
        data.add(new Item( R.drawable.ic_idea_logo, "Idea-Postpaid","IP"));
        data.add(new Item( R.drawable.ic_vodafone, "Vodafone-Postpaid","VP"));
        data.add(new Item( R.drawable.ic_aircel, "Aircel-Postpaid","PAIR"));
        data.add(new Item( R.drawable.ic_docomo, "Docomo-Postpaid","DP"));
        return data;
    }

    public ArrayList<Item> fill_with_data3() {

        ArrayList<Item> data = new ArrayList<>();
        data.add(new Item( R.drawable.ic_airtel_tv, "Airtel Digital Tv","ATV"));
        data.add(new Item( R.drawable.ic_tata_sky, "Tata Sky","TTV"));
        data.add(new Item( R.drawable.ic_dish_tv, "Dish Tv","DTV"));
        data.add(new Item( R.drawable.ic_videocon, "Videocon d2h","VTV"));
        data.add(new Item( R.drawable.ic_big_tv, "Big TV","BTV"));
        data.add(new Item( R.drawable.ic_sun_direct, "Sun Direct","STV"));
        return data;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_main, menu);

        Drawable drawable = menu.findItem(R.id.search).getIcon();
        drawable = DrawableCompat.wrap(drawable);
        DrawableCompat.setTint(drawable, ContextCompat.getColor(getApplicationContext(),android.R.color.white));
        MenuItem search = menu.findItem(R.id.search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(search);
        search(searchView);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void search(SearchView searchView) {

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                mAdapter.getFilter().filter(newText);
                return true;
            }
        });
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
