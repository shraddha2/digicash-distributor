package com.digicash.distributor.Adapter;

/**
 * Created by sai116 on 11/28/2017.
 */

import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.digicash.distributor.DijiCashDistributor;
import com.digicash.distributor.Model.Item;
import com.digicash.distributor.R;

import java.util.List;

public class CustomSpinnerAdapter extends ArrayAdapter<String> {

    private final LayoutInflater mInflater;
    private final List<Item> items;
    private final int mResource;

    public CustomSpinnerAdapter(@LayoutRes int resource,
                                @NonNull List objects) {
        super(DijiCashDistributor.getInstance(), resource, 0, objects);

        mInflater = LayoutInflater.from(DijiCashDistributor.getInstance());
        mResource = resource;
        items = objects;
    }
    @Override
    public View getDropDownView(int position, @Nullable View convertView,
                                @NonNull ViewGroup parent) {
        return createItemView(position, convertView, parent);
    }

    @Override
    public @NonNull
    View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return createItemView(position, convertView, parent);
    }

    private View createItemView(int position, View convertView, ViewGroup parent){
        final View view = mInflater.inflate(mResource, parent, false);

        TextView txt_title = (TextView) view.findViewById(R.id.txt_title);
        TextView txt_id = (TextView) view.findViewById(R.id.txt_id);


        Item offerData = items.get(position);

        txt_title.setText(offerData.getOperator_name());
        txt_id.setText(offerData.getCommission());


        return view;
    }

    public void setError(View v, CharSequence s) {
        TextView name = (TextView) v.findViewById(R.id.txt_title);
        name.setError(s);
    }

}
