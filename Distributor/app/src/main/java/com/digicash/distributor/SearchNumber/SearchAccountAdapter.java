package com.digicash.distributor.SearchNumber;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.constraint.ConstraintLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.digicash.distributor.DefineData;
import com.digicash.distributor.DijiCashDistributor;
import com.digicash.distributor.Model.Item;
import com.digicash.distributor.R;

import java.util.List;

/**
 * Created by admin on 9/8/2018.
 */

public class SearchAccountAdapter extends RecyclerView.Adapter<SearchAccountAdapter.MyViewHolder> {
    // Typeface font ;
    private List<Item> moviesList;
    Context ctx;
    String frg_name="",token="";
    SharedPreferences sharedpreferences;
    String orderId="";

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txt_label1, txt_label2, txt_label3,txt_label4,txt_label5,txt_label6,txt_label7,txt_label8,txt_label9,txt_label10,txt_label11;
        ImageView img_arrow;
        Button img_fail_icon,img_check_icon;
        ConstraintLayout linear_more_data,progress_linear;

        public MyViewHolder(View view) {
            super(view);
            txt_label1 = (TextView) view.findViewById(R.id.txt_label1);
            txt_label2 = (TextView) view.findViewById(R.id.txt_label2);
            txt_label3 = (TextView) view.findViewById(R.id.txt_label3);
            txt_label4 = (TextView) view.findViewById(R.id.txt_label4);
            txt_label5 = (TextView) view.findViewById(R.id.txt_label5);
            txt_label6 = (TextView) view.findViewById(R.id.txt_label6);
            txt_label7 = (TextView) view.findViewById(R.id.txt_label7);
            txt_label8 = (TextView) view.findViewById(R.id.txt_label8);
            txt_label9 = (TextView) view.findViewById(R.id.txt_label9);
            txt_label10 = (TextView) view.findViewById(R.id.txt_label10);
            txt_label11 = (TextView) view.findViewById(R.id.txt_label11);
            img_fail_icon= (Button) view.findViewById(R.id.img_fail_icon);
            img_check_icon= (Button) view.findViewById(R.id.img_check_icon);
            img_arrow= (ImageView) view.findViewById(R.id.img_arrow);
            linear_more_data= (ConstraintLayout) view.findViewById(R.id.linear_more_data);
            progress_linear= (ConstraintLayout) view.findViewById(R.id.loading);

            progress_linear.setVisibility(View.GONE);

            img_fail_icon.setVisibility(View.GONE);
            img_check_icon.setVisibility(View.GONE);
            //txt_label3.setVisibility(View.GONE);

            img_arrow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int visibility =linear_more_data.getVisibility();
                    if (visibility == View.VISIBLE){
                        if(frg_name.equalsIgnoreCase("Wallet History")) {
                            txt_label3.setVisibility(View.GONE);
                        }
                        linear_more_data.setVisibility(View.GONE);
                        img_arrow.setImageResource(R.drawable.ic_expand_more);
                    }else{
                        if(frg_name.equalsIgnoreCase("Wallet History")) {
                            txt_label3.setVisibility(View.VISIBLE);
                        }
                        linear_more_data.setVisibility(View.VISIBLE);
                        img_arrow.setImageResource(R.drawable.ic_expand_less);
                    }
                }
            });
        }
    }

    public SearchAccountAdapter(List<Item> moviesList, Context ctx, String frg_name) {
        this.moviesList = moviesList;
        this.ctx = ctx;
        this.frg_name = frg_name;
        this.sharedpreferences = ctx.getSharedPreferences(DefineData.LOGIN_MINKSPAY_PREFERENCE, Context.MODE_PRIVATE);
        this.token=sharedpreferences.getString(DefineData.TOKEN_KEY,"");
        // font = Typeface.createFromAsset(ctx.getAssets(), "font/Montserrat-Regular.ttf");
    }

    @Override
    public SearchAccountAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.mt_adapter_layout, parent, false);

        return new SearchAccountAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(SearchAccountAdapter.MyViewHolder holder, int position) {
        Item movie = moviesList.get(position);

        holder.txt_label1.setText(movie.getId()+"");
        holder.txt_label2.setText("₹ "+movie.getAmount()+"");
        holder.txt_label3.setText(movie.getSurcharge()+"");
        holder.txt_label4.setText(movie.getTime()+"");
        holder.txt_label5.setText(movie.getMTStatus()+"");
        holder.txt_label6.setText(movie.getSender()+"");
        holder.txt_label7.setText(movie.getBeneficiaryName() + "");
        holder.txt_label8.setText(movie.getRecipientAccount()+"");
        holder.txt_label9.setText(movie.getRecipientMobile());
        holder.txt_label10.setText(movie.getRecipientBank());
        holder.txt_label11.setText(movie.getBankRefNo());

        if(frg_name.equalsIgnoreCase("Search Account Number")) {

            if (movie.getRecipientAccount().equalsIgnoreCase("failed")) {
                holder.txt_label2.setTextColor(ContextCompat.getColor(DijiCashDistributor.getInstance(),R.color.status_fail));
                holder.txt_label8.setTextColor(ContextCompat.getColor(DijiCashDistributor.getInstance(),R.color.status_fail));
            } else if (movie.getRecipientAccount().equalsIgnoreCase("pending")){
                holder.txt_label2.setTextColor(ContextCompat.getColor(DijiCashDistributor.getInstance(),R.color.status_initiated));
                holder.txt_label8.setTextColor(ContextCompat.getColor(DijiCashDistributor.getInstance(),R.color.status_initiated));
            }else if (movie.getRecipientAccount().equalsIgnoreCase("response Pending")){
                holder.txt_label2.setTextColor(ContextCompat.getColor(DijiCashDistributor.getInstance(),R.color.status_initiated));
                holder.txt_label8.setTextColor(ContextCompat.getColor(DijiCashDistributor.getInstance(),R.color.status_initiated));
            }else{
                holder.txt_label2.setTextColor(ContextCompat.getColor(DijiCashDistributor.getInstance(),R.color.status_success));
                holder.txt_label8.setTextColor(ContextCompat.getColor(DijiCashDistributor.getInstance(),R.color.status_success));
            }
        }
    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }
}
