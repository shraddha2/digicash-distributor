package com.digicash.distributor.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import com.digicash.distributor.Model.Item;
import com.digicash.distributor.R;

import java.util.ArrayList;
import java.util.List;

public class AcCustomerAdapter extends ArrayAdapter<Item> {
    List<Item> customers, tempItem, suggestions;

    public AcCustomerAdapter(Context context, List<Item> objects) {
        super(context, android.R.layout.simple_list_item_1, objects);
        this.customers = objects;
        this.tempItem = new ArrayList<Item>(objects);
        this.suggestions = new ArrayList<Item>(objects);

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Item customer = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.spinner_item, parent, false);
        }
        TextView txt_title = (TextView) convertView.findViewById(R.id.txt_title);
        TextView txt_id = (TextView) convertView.findViewById(R.id.txt_id);
        if (txt_title != null)
            txt_title.setText(customer.getOperator_name());
        if (txt_id != null)
            txt_id.setText(customer.getCommission());

        return convertView;
    }

    @Override
    public Filter getFilter() {
        return myFilter;
    }

    Filter myFilter = new Filter() {
        @Override
        public CharSequence convertResultToString(Object resultValue) {
            Item customer = (Item) resultValue;
            return customer.getOperator_name() ;
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            if (constraint != null) {
                suggestions.clear();
                for (Item people : tempItem) {
                    if (people.getOperator_name().toLowerCase().startsWith(constraint.toString().toLowerCase())) {
                        suggestions.add(people);
                    }
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = suggestions;
                filterResults.count = suggestions.size();
                return filterResults;
            } else {
                return new FilterResults();
            }
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            ArrayList<Item> c = (ArrayList<Item>) results.values;
            if (results != null && results.count > 0) {
                clear();
                for (Item cust : c) {
                    add(cust);
                    notifyDataSetChanged();
                }
            }
        }
    };
}