package com.digicash.distributor.Activity;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.SmsManager;
import android.text.InputFilter;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.digicash.distributor.AddRetailer.OperatorCircleActivity;
import com.digicash.distributor.DefineData;
import com.digicash.distributor.DijiCashDistributor;
import com.digicash.distributor.R;
import com.squareup.picasso.Picasso;

import java.util.List;

import static android.Manifest.permission.SEND_SMS;

public class OfflineRechargeActivity extends AppCompatActivity {
    private final int REQUEST_CODE=99;
    String operator_name,operator_id;
    String amt,mobile_number;
    String operator_circle_id="8",operator_circle_name="Maharashtra/Goa";
    Button btn_recharge;
    TextView txt_error,txt_title,txt_label2,edt_operator_circle,txt_label3,txt_label6;
    ImageView img_logo;
    SharedPreferences sharedpreferences;
    SharedPreferences.Editor editor;
    int drawable_icon;
    EditText edt_mobile_number,edt_amt;
    private static final int REQUEST_SMS = 0;
    public  static final int RequestPermissionCode  = 1 ;
    private BroadcastReceiver sentStatusReceiver, deliveredStatusReceiver;
    private static final int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 1;
    int rech_type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offline_recharge);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle("Dijicash");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        sharedpreferences = getSharedPreferences(DefineData.LOGIN_MINKSPAY_PREFERENCE, Context.MODE_PRIVATE);
        editor = sharedpreferences.edit();

        btn_recharge= (Button)findViewById(R.id.btn_recharge);

        txt_error= (TextView) findViewById(R.id.txt_error);
        txt_title= (TextView) findViewById(R.id.txt_title);
        edt_operator_circle= (TextView)findViewById(R.id.edt_operator_circle);
        edt_mobile_number= (EditText)findViewById(R.id.edt_mobile_number);
        edt_amt= (EditText)findViewById(R.id.edt_amt);
        img_logo= (ImageView) findViewById(R.id.img_logo);
        txt_label3= (TextView) findViewById(R.id.txt_label3);
        txt_title.setText("Offline Recharge");
        operator_circle_name=sharedpreferences.getString(DefineData.KEY_OPERATOR_CIRCLE_OFFLINE,"Maharashtra/Goa");
        operator_circle_id=sharedpreferences.getString(DefineData.KEY_OPERATOR_CIRCLE_ID_OFFLINE,"8");
        txt_label2= (TextView) findViewById(R.id.txt_label2);
        txt_label6= (TextView) findViewById(R.id.txt_label6);

        operator_name = getIntent().getStringExtra("operator_name");
        operator_id =  getIntent().getStringExtra("operator_id");
        rech_type =  getIntent().getIntExtra("rech_type",0);
        txt_label2.setText(operator_name + "");
        edt_operator_circle.setText(operator_circle_name + "");

        if(rech_type==3)
        {
            txt_label6.setText("Customer Id");
            edt_mobile_number.setHint("Customer Id");
            edt_mobile_number.setFilters(new InputFilter[] { new InputFilter.LengthFilter(12) });

        }

        ChangeIcon(operator_id);

        txt_label3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent=new Intent(OfflineRechargeActivity.this,OperatorCircleActivity.class);
                intent.putExtra("source","offline");
                startActivityForResult(intent, 3);

            }
        });

        edt_mobile_number.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;

                if(event.getAction() == MotionEvent.ACTION_UP) {
                    if(event.getRawX() >= (edt_mobile_number.getRight() - edt_mobile_number.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {

                        EnableRuntimePermission();

                        // Here, thisActivity is the current activity
                        if (ContextCompat.checkSelfPermission(OfflineRechargeActivity.this,
                                Manifest.permission.READ_CONTACTS)
                                != PackageManager.PERMISSION_GRANTED) {

                            // Should we show an explanation?
                            if (ActivityCompat.shouldShowRequestPermissionRationale(OfflineRechargeActivity.this,
                                    Manifest.permission.READ_CONTACTS)) {

                                // Show an explanation to the user *asynchronously* -- don't block
                                // this thread waiting for the user's response! After the user
                                // sees the explanation, try again to request the permission.

                            } else {

                                // No explanation needed, we can request the permission.

                                ActivityCompat.requestPermissions(OfflineRechargeActivity.this,
                                        new String[]{Manifest.permission.READ_CONTACTS},
                                        MY_PERMISSIONS_REQUEST_READ_CONTACTS);

                                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                                // app-defined int constant. The callback method gets the
                                // result of the request.
                            }
                        }else{
                            Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
                            startActivityForResult(intent, REQUEST_CODE);
                        }

                        return true;
                    }
                }
                return false;
            }
        });

        btn_recharge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                amt= edt_amt.getText().toString();
                mobile_number= edt_mobile_number.getText().toString();
                boolean isError=false;
                if(null==amt||amt.length()==0||amt.equalsIgnoreCase(""))
                {
                    isError=true;
                    edt_amt.setError("Field Cannot be Blank");
                }
                if(null==mobile_number||mobile_number.length()<10||mobile_number.equalsIgnoreCase(""))
                {
                    isError=true;
                    edt_mobile_number.setError("Invalid Mobile Number");
                }

                if(!isError)
                {
                    if(!sharedpreferences.getBoolean(DefineData.KEY_IS_CIRCLE_SAVE_OFFLINE,false)) {
                        final AlertDialog.Builder builder = new AlertDialog.Builder(OfflineRechargeActivity.this);
                        builder.setMessage("Below Operator Circle will Save for Future Transaction \n Operator Circle : "+operator_circle_name);
                        builder.setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int which) {
                                        dialog.dismiss();
                                        custdialog();
                                        //new RechargePrepaid().execute();
                                        editor.putBoolean(DefineData.KEY_IS_CIRCLE_SAVE_OFFLINE, true);
                                        editor.putString(DefineData.KEY_OPERATOR_CIRCLE_OFFLINE, operator_circle_name);
                                        editor.putString(DefineData.KEY_OPERATOR_CIRCLE_ID_OFFLINE, operator_circle_id);
                                        editor.commit();
                                    }
                                });

                        builder.show();

                    }else{
                        //new RechargePrepaid().execute();
                        custdialog();
                    }
                }

            }
        });



    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent i=new Intent(OfflineRechargeActivity.this,OperatorListActivity.class);
        i.putExtra("type",rech_type);
        startActivity(i);
        finish();
    }

    private void custdialog(){
        final View dialogView = View.inflate(this,R.layout.confirm_dialog,null);

        final Dialog dialog = new Dialog(this,R.style.FullScreenDialogStyle);
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
        }
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(dialogView);
        ImageView img_logos=(ImageView)dialog.findViewById(R.id.img_logos);
        TextView txt_amt=(TextView)dialog.findViewById(R.id.txt_amt);
        TextView txt_operator_circle=(TextView)dialog.findViewById(R.id.txt_operator_circle);
        TextView txt_rec_number=(TextView)dialog.findViewById(R.id.txt_rec_number);
        Button btn_confirm = (Button) dialog.findViewById(R.id.btn_confirm);
        Button btn_cancel = (Button) dialog.findViewById(R.id.btn_cancel);
        Picasso.with(DijiCashDistributor.getInstance()).load(drawable_icon).fit().into(img_logos);
        txt_amt.setText("₹ "+amt);
        txt_operator_circle.setText(operator_circle_name);
        txt_rec_number.setText(mobile_number);
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        btn_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                    int hasSMSPermission = checkSelfPermission(SEND_SMS);
                    if (hasSMSPermission != PackageManager.PERMISSION_GRANTED) {
                        if (!shouldShowRequestPermissionRationale(SEND_SMS)) {
                            showMessageOKCancel("You need to allow access to Send SMS",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                requestPermissions(new String[] {SEND_SMS},
                                                        REQUEST_SMS);
                                            }
                                        }
                                    });
                            return;
                        }
                        requestPermissions(new String[] {SEND_SMS},
                                REQUEST_SMS);
                        return;
                    }
                    sendMySMS();
                }else{
                    sendMySMS();
                }


            }
        });
        dialog.show();
    }
    public void sendMySMS() {

        String phone = DefineData.OFFLINE_MOBILE_NUMBER;
        String message = operator_id +" "+operator_circle_id+" "+mobile_number+" "+amt;

        //Check if the phoneNumber is empty
        if (phone.isEmpty()) {
            Toast.makeText(getApplicationContext(), "Please Enter a Valid Phone Number", Toast.LENGTH_SHORT).show();
        } else {

            SmsManager sms = SmsManager.getDefault();
            // if message length is too long messages are divided
            List<String> messages = sms.divideMessage(message);
            for (String msg : messages) {

                PendingIntent sentIntent = PendingIntent.getBroadcast(this, 0, new Intent("SMS_SENT"), 0);
                PendingIntent deliveredIntent = PendingIntent.getBroadcast(this, 0, new Intent("SMS_DELIVERED"), 0);
                sms.sendTextMessage(phone, null, msg, sentIntent, deliveredIntent);

            }
        }
    }
    private void ChangeIcon(String id)
    {
        switch(id)
        {
            case "A":
                drawable_icon=R.drawable.ic_airtel;
                Picasso.with(this).load(R.drawable.ic_airtel).fit().into(img_logo);
                break;
            case "AIR":
                drawable_icon=R.drawable.ic_aircel;
                Picasso.with(this).load(R.drawable.ic_aircel).fit().into(img_logo);
                break;
            case "I":
                drawable_icon=R.drawable.ic_idea_logo;
                Picasso.with(this).load(R.drawable.ic_idea_logo).fit().into(img_logo);
                break;
            case "V":
                drawable_icon=R.drawable.ic_vodafone;
                Picasso.with(this).load(R.drawable.ic_vodafone).fit().into(img_logo);
                break;
            case "BT":
                drawable_icon=R.drawable.ic_bsnl;
                Picasso.with(this).load(R.drawable.ic_bsnl).fit().into(img_logo);
                break;
            case "BS":
                drawable_icon=R.drawable.ic_bsnl;
                Picasso.with(this).load(R.drawable.ic_bsnl).fit().into(img_logo);
                break;
            case "U":
                drawable_icon=R.drawable.ic_telenor;
                Picasso.with(this).load(R.drawable.ic_telenor).fit().into(img_logo);
                break;
            case "US":
                drawable_icon=R.drawable.ic_telenor;
                Picasso.with(this).load(R.drawable.ic_telenor).fit().into(img_logo);
                break;
            case "RJ":
                drawable_icon=R.drawable.ic_jio;
                Picasso.with(this).load(R.drawable.ic_jio).fit().into(img_logo);
                break;
            case "D":
                drawable_icon=R.drawable.ic_docomo;
                Picasso.with(this).load(R.drawable.ic_docomo).fit().into(img_logo);
                break;
            case "DS":
                drawable_icon=R.drawable.ic_docomo;
                Picasso.with(this).load(R.drawable.ic_docomo).fit().into(img_logo);
                break;
            case "MTT":
                drawable_icon=R.drawable.ic_mtnl;
                Picasso.with(this).load(R.drawable.ic_mtnl).fit().into(img_logo);
                break;
            case "MTR":
                drawable_icon=R.drawable.ic_mtnl;
                Picasso.with(this).load(R.drawable.ic_mtnl).fit().into(img_logo);
                break;
            case "AP":
                drawable_icon=R.drawable.ic_airtel;
                Picasso.with(this).load(R.drawable.ic_airtel).fit().into(img_logo);
                break;
            case "IP":
                drawable_icon=R.drawable.ic_idea_logo;
                Picasso.with(this).load(R.drawable.ic_idea_logo).fit().into(img_logo);
                break;
            case "VP":
                drawable_icon=R.drawable.ic_vodafone;
                Picasso.with(this).load(R.drawable.ic_vodafone).fit().into(img_logo);
                break;
            case "PAIR":
                drawable_icon=R.drawable.ic_aircel;
                Picasso.with(this).load(R.drawable.ic_aircel).fit().into(img_logo);
                break;
            case "DP":
                drawable_icon=R.drawable.ic_docomo;
                Picasso.with(this).load(R.drawable.ic_docomo).fit().into(img_logo);
                break;
            case "ATV":
                drawable_icon=R.drawable.ic_airtel_tv;
                Picasso.with(this).load(R.drawable.ic_airtel_tv).fit().into(img_logo);
                break;
            case "TTV":
                drawable_icon=R.drawable.ic_tata_sky;
                Picasso.with(this).load(R.drawable.ic_tata_sky).fit().into(img_logo);
                break;
            case "DTV":
                drawable_icon=R.drawable.ic_dish_tv;
                Picasso.with(this).load(R.drawable.ic_dish_tv).fit().into(img_logo);
                break;
            case "VTV":
                drawable_icon=R.drawable.ic_videocon;
                Picasso.with(this).load(R.drawable.ic_videocon).fit().into(img_logo);
                break;
            case "BTV":
                drawable_icon=R.drawable.ic_big_tv;
                Picasso.with(this).load(R.drawable.ic_big_tv).fit().into(img_logo);
                break;
            case "STV":
                drawable_icon=R.drawable.ic_sun_direct;
                Picasso.with(this).load(R.drawable.ic_sun_direct).fit().into(img_logo);
                break;
            default:   drawable_icon=R.drawable.ic_airtel;
                Picasso.with(this).load(R.drawable.ic_airtel).fit().into(img_logo);
        }

    }

    @Override
    public void onActivityResult(int reqCode, int resultCode, Intent data) {
        super.onActivityResult(reqCode, resultCode, data);

        switch (reqCode) {
            case (REQUEST_CODE):
                if (resultCode == Activity.RESULT_OK) {
                    Uri contactData = data.getData();
                    Cursor c =getContentResolver().query(contactData, null, null, null, null);
                    if (c.moveToFirst()) {
                        String contactId = c.getString(c.getColumnIndex(ContactsContract.Contacts._ID));
                        String hasNumber = c.getString(c.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER));
                        String num = "";
                        if (Integer.valueOf(hasNumber) == 1) {
                            Cursor numbers = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + contactId, null, null);
                            while (numbers.moveToNext()) {
                                num = numbers.getString(numbers.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                                edt_mobile_number.setText(phoeNumberWithOutCountryCode(num)+ "");
                                // Toast.makeText(getActivity(), "Number="+num, Toast.LENGTH_LONG).show();
                            }
                        }
                    }
                }
                break;
            case 2:
                if (null!=data) {
                    operator_id = data.getStringExtra("operator_id");
                    operator_name = data.getStringExtra("operator_name");
                    txt_label2.setText(operator_name + "");
                    ChangeIcon(operator_id);
                }
                break;
            case 3:
                if (null!=data) {
                    operator_circle_id = data.getStringExtra("operator_circle_id");
                    operator_circle_name = data.getStringExtra("operator_circle_name");
                    edt_operator_circle.setText(operator_circle_name + "");

                }
                break;

        }}

    public String phoeNumberWithOutCountryCode(String num) {
        if (num.startsWith("+91")) {
            num = num.replaceFirst("\\+(91)", "");}
        return num;
    }

    public void EnableRuntimePermission(){

        if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.READ_CONTACTS))
        {
            Toast.makeText(this,"CONTACTS permission allows us to Access CONTACTS app", Toast.LENGTH_LONG).show();
            ActivityCompat.requestPermissions(this,new String[]{
                    Manifest.permission.READ_CONTACTS}, RequestPermissionCode);


        } else {

            ActivityCompat.requestPermissions(this,new String[]{
                    Manifest.permission.READ_CONTACTS}, RequestPermissionCode);

        }
    }


    public void onResume() {
        super.onResume();
        sentStatusReceiver=new BroadcastReceiver() {

            @Override
            public void onReceive(Context arg0, Intent arg1) {
                String s = "Unknown Error";
                switch (getResultCode()) {
                    case Activity.RESULT_OK:
                        s = "Message Sent Successfully !!";
                        break;
                    case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                        s = "Generic Failure Error";
                        break;
                    case SmsManager.RESULT_ERROR_NO_SERVICE:
                        s = "Error : No Service Available";
                        break;
                    case SmsManager.RESULT_ERROR_NULL_PDU:
                        s = "Error : Null PDU";
                        break;
                    case SmsManager.RESULT_ERROR_RADIO_OFF:
                        s = "Error : Radio is off";
                        break;
                    default:
                        break;
                }

                Intent i=new Intent(OfflineRechargeActivity.this, SuccessActivity.class);
                i.putExtra("msg",s+"");
                i.putExtra("type","Offline Recharge");
                startActivity(i);
                finish();


            }
        };
        deliveredStatusReceiver=new BroadcastReceiver() {

            @Override
            public void onReceive(Context arg0, Intent arg1) {
                String s = "Message Not Delivered";
                switch(getResultCode()) {
                    case Activity.RESULT_OK:
                        s = "Message Delivered Successfully";
                        break;
                    case Activity.RESULT_CANCELED:
                        break;
                }
                txt_error.setText(s);
                edt_mobile_number.setText("");
                edt_amt.setText("");
                txt_error.setVisibility(View.VISIBLE);

            }
        };
        registerReceiver(sentStatusReceiver, new IntentFilter("SMS_SENT"));
        //registerReceiver(deliveredStatusReceiver, new IntentFilter("SMS_DELIVERED"));
    }


    public void onPause() {
        super.onPause();
        unregisterReceiver(sentStatusReceiver);
        //unregisterReceiver(deliveredStatusReceiver);
    }



    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_SMS:
                if (grantResults.length > 0 &&  grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    Toast.makeText(getApplicationContext(), "Permission Granted, Now you can access sms", Toast.LENGTH_SHORT).show();
                    sendMySMS();

                }else {
                    Toast.makeText(getApplicationContext(), "Permission Denied, You cannot access and sms", Toast.LENGTH_SHORT).show();
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (shouldShowRequestPermissionRationale(SEND_SMS)) {
                            showMessageOKCancel("You need to allow access to both the permissions",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                requestPermissions(new String[]{SEND_SMS},
                                                        REQUEST_SMS);
                                            }
                                        }
                                    });
                            return;
                        }
                    }
                }
                break;
            case MY_PERMISSIONS_REQUEST_READ_CONTACTS: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
                    startActivityForResult(intent, REQUEST_CODE);

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(OfflineRechargeActivity.this,"Please enable permission to access contact ", Toast.LENGTH_LONG).show();
                }
                return;
            }

            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new android.support.v7.app.AlertDialog.Builder(OfflineRechargeActivity.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //unregisterReceiver(sentStatusReceiver);
    }
}
