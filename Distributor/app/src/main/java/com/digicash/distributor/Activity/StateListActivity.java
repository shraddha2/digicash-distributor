package com.digicash.distributor.Activity;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.digicash.distributor.Adapter.StateListAdapter;
import com.digicash.distributor.Model.DividerItemDecoration;
import com.digicash.distributor.Model.Item;
import com.digicash.distributor.R;

import java.util.ArrayList;

public class StateListActivity extends AppCompatActivity {

    private ArrayList<Item> movieList = new ArrayList<>();
    private RecyclerView recyclerView;
    private StateListAdapter mAdapter;
    private FirebaseAnalytics mFirebaseAnalytics;
    String source="";
    TextView txt_error, txt_network_msg, txt_err_msgg;
    LinearLayout progress_linear,linear_container;
    RelativeLayout rel_no_records,rel_no_internet;
    Button btn_refresh;
    SwipeRefreshLayout mSwipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_state_list);

        // Obtain the FirebaseAnalytics instance.
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        setTitle("Select Bank");
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        txt_error= (TextView) findViewById(R.id.txt_error);
        recyclerView = (RecyclerView) findViewById(R.id.rc_retailers);

        txt_err_msgg= (TextView) findViewById(R.id.txt_err_msgg);
        txt_network_msg= (TextView) findViewById(R.id.txt_network_msg);

        progress_linear= (LinearLayout) findViewById(R.id.loding);
        linear_container= (LinearLayout) findViewById(R.id.container);
        rel_no_records= (RelativeLayout) findViewById(R.id.rel_no_records);
        rel_no_internet= (RelativeLayout) findViewById(R.id.rel_no_internet);
        btn_refresh= (Button) findViewById(R.id.btn_refresh);
        mSwipeRefreshLayout= (SwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout);

        source=getIntent().getStringExtra("source");

        movieList = fill_with_data2();
        mAdapter = new StateListAdapter(movieList,this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new DividerItemDecoration(StateListActivity.this));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

        progress_linear.setVisibility(View.GONE);
        linear_container.setVisibility(View.VISIBLE);
        rel_no_records.setVisibility(View.GONE);
        rel_no_internet.setVisibility(View.GONE);
        txt_network_msg.setVisibility(View.GONE);
        txt_err_msgg.setVisibility(View.GONE);
    }

    public ArrayList<Item> fill_with_data2() {

        ArrayList<Item> list_banks = new ArrayList<>();
        //   list_banks.add(0,new Item("--Select--","0"));

        list_banks.add(new Item("Maharashtra","MHA"));
        list_banks.add(new Item("Goa","GOA"));
        list_banks.add(new Item("Tamilnadu","TNDU"));
        list_banks.add(new Item("Gujarat","GR"));
        list_banks.add(new Item("Bihar","BR"));
        list_banks.add(new Item("Haryana","HNA"));
        list_banks.add(new Item("Andhra Pradesh","AP"));
        list_banks.add(new Item("Visakhapatanam","VSPNA"));
        list_banks.add(new Item("Kerala","KLA"));
        list_banks.add(new Item("Uttar Pradesh","UP"));
        list_banks.add(new Item("Madhya Pradesh","MP"));
        list_banks.add(new Item("Telangana","TGNA"));
        list_banks.add(new Item("Delhi","DLI"));
        list_banks.add(new Item("Rajasthan","RTAN"));
        list_banks.add(new Item("West bengal","WB"));
        return list_banks;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_main, menu);

        Drawable drawable = menu.findItem(R.id.search).getIcon();
        drawable = DrawableCompat.wrap(drawable);
        DrawableCompat.setTint(drawable, ContextCompat.getColor(this,android.R.color.white));

        MenuItem search = menu.findItem(R.id.search);

        SearchView searchView = (SearchView) MenuItemCompat.getActionView(search);
        search(searchView);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void search(SearchView searchView) {

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                mAdapter.getFilter().filter(newText);
                return true;
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        Intent intent=new Intent();
        setResult(5,intent);
        finish();//finishing activity

    }
}
