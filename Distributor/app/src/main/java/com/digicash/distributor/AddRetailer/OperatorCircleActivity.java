package com.digicash.distributor.AddRetailer;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.digicash.distributor.Adapter.OperatorCircleAdapter;
import com.digicash.distributor.Model.DividerItemDecoration;
import com.digicash.distributor.Model.Item;
import com.digicash.distributor.R;

import java.util.ArrayList;

public class OperatorCircleActivity extends AppCompatActivity {
    private ArrayList<Item> movieList = new ArrayList<>();
    private RecyclerView recyclerView;
    private OperatorCircleAdapter mAdapter;
    private FirebaseAnalytics mFirebaseAnalytics;
    String source="";
    TextView txt_error, txt_network_msg, txt_err_msgg;
    ConstraintLayout progress_linear,linear_container,rel_no_records,rel_no_internet;
    Button btn_refresh;
    SwipeRefreshLayout mSwipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_operator_circle);

        // Obtain the FirebaseAnalytics instance.
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle("Select Operator");
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        txt_error= (TextView) findViewById(R.id.txt_error);
        recyclerView = (RecyclerView) findViewById(R.id.rc_retailers);

        txt_err_msgg= (TextView) findViewById(R.id.txt_err_msgg);
        txt_network_msg= (TextView) findViewById(R.id.txt_network_msg);

        progress_linear= (ConstraintLayout) findViewById(R.id.loading);
        linear_container= (ConstraintLayout) findViewById(R.id.container);
        rel_no_records= (ConstraintLayout) findViewById(R.id.rel_no_records);
        rel_no_internet= (ConstraintLayout) findViewById(R.id.rel_no_internet);
        //btn_refresh= (Button) findViewById(R.id.btn_refresh);
        mSwipeRefreshLayout= (SwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout);

        source=getIntent().getStringExtra("source");

        movieList = fill_with_data2();
        mAdapter = new OperatorCircleAdapter(movieList,this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new DividerItemDecoration(OperatorCircleActivity.this));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

        progress_linear.setVisibility(View.GONE);
        linear_container.setVisibility(View.VISIBLE);
        rel_no_records.setVisibility(View.GONE);
        rel_no_internet.setVisibility(View.GONE);
        txt_network_msg.setVisibility(View.GONE);
        txt_err_msgg.setVisibility(View.GONE);
    }

    public ArrayList<Item> fill_with_data2() {

        ArrayList<Item> data = new ArrayList<>();
        data.add(new Item( "Andhra Pradesh","1"));
        data.add(new Item( "Bihar","2"));
        data.add(new Item( "Gujarat","3"));
        data.add(new Item( "Haryana","4"));
        data.add(new Item(  "Karnataka","5"));
        data.add(new Item( "Kerala","6"));
        data.add(new Item( "Madhya Pradesh","7"));
        data.add(new Item( "Maharashtra & Goa","8"));
        data.add(new Item( "Orissa","9"));
        data.add(new Item( "Rajasthan","10"));
        data.add(new Item( "Tamil Nadu","11"));
        data.add(new Item( "Uttar Pradesh (East)","12"));
        data.add(new Item( "Uttar Pradesh (West)","13"));
        data.add(new Item( "West Bengal","14"));
        data.add(new Item( "Northeast","15"));
        data.add(new Item( "Assam","16"));
        data.add(new Item( "Punjab","17"));
        data.add(new Item( "Delhi","18"));
        data.add(new Item( "Himachal Pradesh","19"));
        data.add(new Item( "Jammu and Kashmir","20"));
        data.add(new Item( "Mumbai","21"));
        data.add(new Item( "Kolkatta","22"));
        return data;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_main, menu);

        Drawable drawable = menu.findItem(R.id.search).getIcon();
        drawable = DrawableCompat.wrap(drawable);
        DrawableCompat.setTint(drawable, ContextCompat.getColor(this,android.R.color.white));

        MenuItem search = menu.findItem(R.id.search);

        SearchView searchView = (SearchView) MenuItemCompat.getActionView(search);
        search(searchView);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void search(SearchView searchView) {

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                mAdapter.getFilter().filter(newText);
                return true;
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        Intent intent=new Intent();
        setResult(5,intent);
        finish();//finishing activity

    }
}
