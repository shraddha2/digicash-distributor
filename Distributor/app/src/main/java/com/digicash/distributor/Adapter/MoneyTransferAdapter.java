package com.digicash.distributor.Adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.digicash.distributor.DijiCashDistributor;
import com.digicash.distributor.Model.Item;
import com.digicash.distributor.R;

import java.util.ArrayList;
import java.util.List;

public class MoneyTransferAdapter extends RecyclerView.Adapter<MoneyTransferAdapter.MyViewHolder> implements Filterable {

    private List<Item> mArrayList;
    private List<Item> mFilteredList;
    Context ctx;
    String frg_name = "";

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txt_label1, txt_label2, txt_label3, txt_label4, txt_label5, txt_label6, txt_label7, txt_label8;
        ImageView img_arrow, img_logo;
        LinearLayout linear_more_data;

        //Typeface font ;
        public MyViewHolder(View view) {
            super(view);
            txt_label1 = (TextView) view.findViewById(R.id.txt_label1);
            txt_label2 = (TextView) view.findViewById(R.id.txt_label2);
            txt_label3 = (TextView) view.findViewById(R.id.txt_label3);
            txt_label4 = (TextView) view.findViewById(R.id.txt_label4);
            txt_label5 = (TextView) view.findViewById(R.id.txt_label5);
            txt_label6 = (TextView) view.findViewById(R.id.txt_label6);
            txt_label7 = (TextView) view.findViewById(R.id.txt_label7);
            txt_label8 = (TextView) view.findViewById(R.id.txt_label8);
            img_logo = (ImageView) view.findViewById(R.id.img_logo);
            img_arrow = (ImageView) view.findViewById(R.id.img_arrow);
            linear_more_data = (LinearLayout) view.findViewById(R.id.linear_more_data);
            txt_label4.setVisibility(View.GONE);
            txt_label6.setVisibility(View.GONE);
            txt_label5.setVisibility(View.GONE);
            img_arrow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int visibility = linear_more_data.getVisibility();
                    if (visibility == View.VISIBLE) {
                        linear_more_data.setVisibility(View.GONE);
                        img_arrow.setImageResource(R.drawable.ic_expand_more);
                    } else {

                        linear_more_data.setVisibility(View.VISIBLE);
                        img_arrow.setImageResource(R.drawable.ic_expand_less);
                    }
                }
            });

        }
    }


    public MoneyTransferAdapter(List<Item> moviesList, Context ctx, String frg_name) {
        this.mArrayList = moviesList;
        this.mFilteredList = moviesList;
        this.ctx = ctx;
        this.frg_name = frg_name;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.money_transfer, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Item movie = mFilteredList.get(position);

        if (movie.getStatus().equalsIgnoreCase("Failed")) {
            holder.txt_label2.setTextColor(ContextCompat.getColor(DijiCashDistributor.getInstance(), R.color.status_fail));
            holder.txt_label8.setTextColor(ContextCompat.getColor(DijiCashDistributor.getInstance(), R.color.status_fail));
        } else if (movie.getStatus().equalsIgnoreCase("Processing")) {
            holder.txt_label2.setTextColor(ContextCompat.getColor(DijiCashDistributor.getInstance(), R.color.status_process));
            holder.txt_label8.setTextColor(ContextCompat.getColor(DijiCashDistributor.getInstance(), R.color.status_process));
        } else {
            holder.txt_label2.setTextColor(ContextCompat.getColor(DijiCashDistributor.getInstance(), R.color.status_success));
            holder.txt_label8.setTextColor(ContextCompat.getColor(DijiCashDistributor.getInstance(), R.color.status_success));
        }

    }

    @Override
    public int getItemCount() {
        return mFilteredList.size();
    }

    @Override
    public Filter getFilter() {

        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {

                String charString = charSequence.toString();

                if (charString.isEmpty()) {

                    mFilteredList = mArrayList;
                } else {

                    ArrayList<Item> filteredList = new ArrayList<>();

                    for (Item androidVersion : mArrayList) {

                    }

                    mFilteredList = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = mFilteredList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                mFilteredList = (ArrayList<Item>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }
}
