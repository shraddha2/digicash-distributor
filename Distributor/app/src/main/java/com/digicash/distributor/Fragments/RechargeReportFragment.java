package com.digicash.distributor.Fragments;


import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.digicash.distributor.Adapter.RechargeReportAdapter;
import com.digicash.distributor.DefineData;
import com.digicash.distributor.DijiCashDistributor;
import com.digicash.distributor.HomePage.HomeFragment;
import com.digicash.distributor.Model.DividerItemDecoration;
import com.digicash.distributor.Model.HTTPURLConnection;
import com.digicash.distributor.Model.Item;
import com.digicash.distributor.R;
import com.digicash.distributor.Receiver.ConnectivityReceiver;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

public class RechargeReportFragment extends Fragment implements ConnectivityReceiver.ConnectivityReceiverListener {
    private ArrayList<Item> movieList = new ArrayList<>();
    private RecyclerView recyclerView;
    private RechargeReportAdapter mAdapter;
    Context ctx;
    String token;
    SharedPreferences sharedpreferences;
    TextView txt_title,txt_label1,txt_err_msgg,txt_network_msg;
    LinearLayout progress_linear,linear_container;
    RelativeLayout rel_no_records,rel_no_internet;

    public RechargeReportFragment()  {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView=inflater.inflate(R.layout.fragment_retailer_recharges, container, false);
        setHasOptionsMenu(true);
        ctx=getActivity();

        sharedpreferences = getActivity().getSharedPreferences(DefineData.LOGIN_MINKSPAY_PREFERENCE, Context.MODE_PRIVATE);
        token=sharedpreferences.getString(DefineData.TOKEN_KEY,"");

        txt_title= (TextView) rootView.findViewById(R.id.txt_title);

        txt_err_msgg= (TextView) rootView.findViewById(R.id.txt_err_msgg);
        txt_network_msg= (TextView) rootView.findViewById(R.id.txt_network_msg);

        progress_linear= (LinearLayout) rootView.findViewById(R.id.loding);
        linear_container= (LinearLayout) rootView.findViewById(R.id.container);
        rel_no_records= (RelativeLayout) rootView.findViewById(R.id.rel_no_records);
        rel_no_internet= (RelativeLayout) rootView.findViewById(R.id.rel_no_internet);

        txt_title.setText("Recharges Report");

        recyclerView = (RecyclerView) rootView.findViewById(R.id.rc_retailers);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(ctx);
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity()));
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        mAdapter = new RechargeReportAdapter(movieList,ctx,"Recharges Report");
        recyclerView.setAdapter(mAdapter);

        if(checkConnection()) {
            linear_container.setVisibility(View.GONE);
            rel_no_records.setVisibility(View.GONE);
            progress_linear.setVisibility(View.GONE);
            rel_no_internet.setVisibility(View.GONE);
            new FetchRetailerRecharges().execute();
        }else{
            linear_container.setVisibility(View.GONE);
            rel_no_records.setVisibility(View.GONE);
            progress_linear.setVisibility(View.GONE);
            rel_no_internet.setVisibility(View.VISIBLE);
        }

        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(final Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.wallet_history_menu, menu);
        Drawable drawable2 = menu.findItem(R.id.action_search).getIcon();
        drawable2 = DrawableCompat.wrap(drawable2);
        DrawableCompat.setTint(drawable2, ContextCompat.getColor(ctx,android.R.color.white));

        Drawable drawable = menu.findItem(R.id.action_filter).getIcon();
        drawable = DrawableCompat.wrap(drawable);
        DrawableCompat.setTint(drawable, ContextCompat.getColor(ctx,android.R.color.white));

        MenuItem search = menu.findItem(R.id.action_search);
        final MenuItem filter = menu.findItem(R.id.action_filter);
        final View menuItemView = getActivity().findViewById(R.id.action_filter);

        SearchView searchView = (SearchView) search.getActionView();
        search(searchView);

        super.onCreateOptionsMenu(menu, inflater);
    }

    private void search(SearchView searchView) {

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                mAdapter.getFilter().filter(newText);
                return true;
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.monthly_rep:
                showDiag();
                return false;
            case R.id.clear:
                new FetchRetailerRecharges().execute();
                return false;
            default:
                break;
        }

        return false;
    }



    @Override
    public void onResume() {

        super.onResume();
        DijiCashDistributor.getInstance().setConnectivityListener(this);

        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK){

                    Fragment frg=new HomeFragment();
                    ((FragmentActivity) ctx).getFragmentManager().beginTransaction()
                            .replace(R.id.frg_replace, frg)
                            .addToBackStack(null)
                            .commit();

                    return true;

                }

                return false;
            }
        });
    }
    private class FetchRetailerRecharges extends AsyncTask<Void, Void, Void> {

        JSONObject response;
        @Override
        protected void onPreExecute() {
            movieList.clear();
            linear_container.setVisibility(View.GONE);
            rel_no_records.setVisibility(View.GONE);
            progress_linear.setVisibility(View.VISIBLE);

        }
        @Override
        protected Void doInBackground(Void... params) {
            HTTPURLConnection service = new HTTPURLConnection();
            try{

                this.response = new JSONObject(service.POST(DefineData.FETCH_RECHARGES_REPORT,token));
            }catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            Log.d("res_search_recharges",response+"");
            if(response!=null) {
                try {
                    if (response.getBoolean("error")) {
                        txt_err_msgg.setText("oops ! Server Error");
                        linear_container.setVisibility(View.GONE);
                        rel_no_records.setVisibility(View.VISIBLE);
                        progress_linear.setVisibility(View.GONE);
                    } else {
                        JSONArray jsonArray = response.getJSONArray("data");
                        if(jsonArray.length()!=0) {
                            for (int i = 0; i < jsonArray.length(); i++) {
                                Item superHero = null;
                                JSONObject json2 = null;
                                try {
                                    //Getting json
                                    json2 = jsonArray.getJSONObject(i);

                                    String id = json2.getString("id");
                                    String mobileno = json2.getString("number");
                                    String amount = json2.getString("amount");
                                    String transaction_no = json2.getString("transaction_no");
                                    String commission = json2.getString("dist_comm");
                                    String operator_id = json2.getString("operator_id");
                                    String status = json2.getString("status");
                                    String time = json2.getString("time");
                                    superHero = new Item( id,  mobileno, amount, transaction_no, commission,operator_id, status,DefineData.parseDateToddMMyyyyhh(time)) ;

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    txt_err_msgg.setText("oops ! Server Error");
                                    linear_container.setVisibility(View.GONE);
                                    rel_no_records.setVisibility(View.VISIBLE);
                                    progress_linear.setVisibility(View.GONE);

                                }

                                movieList.add(superHero);
                            }
                            mAdapter.notifyDataSetChanged();
                            linear_container.setVisibility(View.VISIBLE);
                            rel_no_records.setVisibility(View.GONE);
                            progress_linear.setVisibility(View.GONE);
                        }else{
                            txt_err_msgg.setText("No Records Found!");
                            linear_container.setVisibility(View.GONE);
                            rel_no_records.setVisibility(View.VISIBLE);
                            progress_linear.setVisibility(View.GONE);
                        }

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    txt_err_msgg.setText("oops ! Server Error");
                    linear_container.setVisibility(View.GONE);
                    rel_no_records.setVisibility(View.VISIBLE);
                    progress_linear.setVisibility(View.GONE);

                }
            }else{
                txt_err_msgg.setText("oops ! Server Error");
                linear_container.setVisibility(View.GONE);
                rel_no_records.setVisibility(View.VISIBLE);
                progress_linear.setVisibility(View.GONE);
            }

        }
    }

    // Method to manually check connection status
    private boolean checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        return isConnected;
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {

    }

    private void showDiag() {

        final View dialogView = View.inflate(ctx,R.layout.custom_filter,null);

        final Dialog dialog = new Dialog(ctx,R.style.MyAlertDialogStyle);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(dialogView);

        final TextView txt_from_date  = (TextView) dialog.findViewById(R.id.txt_from_date);
        final TextView txt_to_date  = (TextView) dialog.findViewById(R.id.txt_to_date);
        ImageView btn_search  = (ImageView) dialog.findViewById(R.id.btn_search);
        txt_to_date.setText(DefineData.parseDateToddMMyyyy(DefineData.getCurrentDate()));
        txt_from_date.setText(DefineData.parseDateToddMMyyyy(DefineData.getCurrentDate()));

        txt_from_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar mcurrentDate=Calendar.getInstance();
                int mYear=mcurrentDate.get(Calendar.YEAR);
                int mMonth=mcurrentDate.get(Calendar.MONTH);
                int mDay=mcurrentDate.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog mDatePicker=new DatePickerDialog(getActivity(),new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                        selectedmonth=selectedmonth+1;
                        String inputDateStr=selectedyear+"-"+selectedmonth+"-"+selectedday;
                        txt_from_date.setText(DefineData.parseDateToddMMyyyy(inputDateStr));

                    }
                },mYear, mMonth, mDay);
                mDatePicker.getDatePicker().setMaxDate(System.currentTimeMillis());
                mDatePicker.setTitle("Start Date");
                mDatePicker.show();  }


        });


        txt_to_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar mcurrentDate=Calendar.getInstance();
                int mYear=mcurrentDate.get(Calendar.YEAR);
                int mMonth=mcurrentDate.get(Calendar.MONTH);
                int mDay=mcurrentDate.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog mDatePicker=new DatePickerDialog(getActivity(),new DatePickerDialog.OnDateSetListener() {
                    public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                        selectedmonth=selectedmonth+1;
                        String inputDateStr=selectedyear+"-"+selectedmonth+"-"+selectedday;
                        txt_to_date.setText(DefineData.parseDateToddMMyyyy(inputDateStr));

                    }
                },mYear, mMonth, mDay);
                mDatePicker.getDatePicker().setMaxDate(System.currentTimeMillis());
                mDatePicker.setTitle("End Date");
                mDatePicker.show();  }


        });

        btn_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String from_date=txt_from_date.getText().toString();
                final String to_date=txt_to_date.getText().toString();
                boolean isError=false;
                if(null==from_date||from_date.length()==0||from_date.equalsIgnoreCase(""))
                {
                    isError=true;
                    txt_from_date.setError("Field Cannot be blank");
                }
                if(null==to_date||to_date.length()==0||to_date.equalsIgnoreCase(""))
                {
                    isError=true;
                    txt_to_date.setError("Field Cannot be blank");
                }
                if(!isError)
                {
                    dialog.dismiss();
                    new FetchRechargesByDate().execute(from_date,to_date);

                }

            }
        });

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        dialog.show();
    }

    private class FetchRechargesByDate extends AsyncTask<String, Void, Void> {

        JSONObject response;
        @Override
        protected void onPreExecute() {
            movieList.clear();
            linear_container.setVisibility(View.GONE);
            rel_no_records.setVisibility(View.GONE);
            progress_linear.setVisibility(View.VISIBLE);
        }
        @Override
        protected Void doInBackground(String... params) {
            HTTPURLConnection service = new HTTPURLConnection();
            try{

                HashMap<String, String> parameters = new HashMap<String, String>();
                parameters.put("start_date", DefineData.parseDateToyyyMMdd(params[0]));
                parameters.put("end_date", DefineData.parseDateToyyyMMdd(params[1]));
                this.response = new JSONObject(service.POST(DefineData.FETCH_RECHARGES_REPORT_DATE,parameters,token));
            }catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            Log.d("res_search_recharges",response+"");
            if(response!=null) {
                try {
                    if (response.getBoolean("error")) {
                        txt_err_msgg.setText("oops ! Server Error");
                        linear_container.setVisibility(View.GONE);
                        rel_no_records.setVisibility(View.VISIBLE);
                        progress_linear.setVisibility(View.GONE);
                    } else {
                        JSONArray jsonArray = response.getJSONArray("data");
                        if(jsonArray.length()!=0) {
                            for (int i = 0; i < jsonArray.length(); i++) {
                                Item superHero = null;
                                JSONObject json2 = null;
                                try {
                                    //Getting json
                                    json2 = jsonArray.getJSONObject(i);
                                    String id = json2.getString("id");
                                    String mobileno = json2.getString("number");
                                    String amount = json2.getString("amount");
                                    String transaction_no = json2.getString("transaction_no");
                                    String commission = json2.getString("dist_comm");
                                    String operator_id = json2.getString("operator_id");
                                    String status = json2.getString("status");
                                    String time = json2.getString("time");
                                    superHero = new Item( id,  mobileno, amount, transaction_no, commission,operator_id, status,DefineData.parseDateToddMMyyyyhh(time)) ;

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    txt_err_msgg.setText("oops ! Server Error");
                                    linear_container.setVisibility(View.GONE);
                                    rel_no_records.setVisibility(View.VISIBLE);
                                    progress_linear.setVisibility(View.GONE);
                                }
                                movieList.add(superHero);
                            }
                            mAdapter.notifyDataSetChanged();
                            linear_container.setVisibility(View.VISIBLE);
                            rel_no_records.setVisibility(View.GONE);
                            progress_linear.setVisibility(View.GONE);
                        }else{
                            txt_err_msgg.setText("Sorry ! Recharge Not Available");
                            linear_container.setVisibility(View.GONE);
                            rel_no_records.setVisibility(View.VISIBLE);
                            progress_linear.setVisibility(View.GONE);
                        }

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    txt_err_msgg.setText("oops ! Server Error");
                    linear_container.setVisibility(View.GONE);
                    rel_no_records.setVisibility(View.VISIBLE);
                    progress_linear.setVisibility(View.GONE);
                }
            }else{
                txt_err_msgg.setText("oops ! Server Error");
                linear_container.setVisibility(View.GONE);
                rel_no_records.setVisibility(View.VISIBLE);
                progress_linear.setVisibility(View.GONE);
            }
        }
    }
}
