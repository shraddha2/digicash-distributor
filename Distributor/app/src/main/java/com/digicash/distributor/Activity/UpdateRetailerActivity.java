package com.digicash.distributor.Activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.layer_net.stepindicator.StepIndicator;
import com.digicash.distributor.R;

public class UpdateRetailerActivity extends AppCompatActivity {

    private SectionsPagerAdapter mSectionsPagerAdapter;
    private static ViewPager mViewPager;
    private FirebaseAnalytics mFirebaseAnalytics;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_fos);

        // Obtain the FirebaseAnalytics instance.
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        StepIndicator stepIndicator = (StepIndicator) findViewById(R.id.step_indicator);
        stepIndicator.setupWithViewPager(mViewPager);

    }

    public static class PlaceholderFragment extends Fragment {
        private static final String ARG_SECTION_NUMBER = "section_number";

        public PlaceholderFragment() {
        }

        public static PlaceholderFragment newInstance(int sectionNumber,String pagetitle) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            args.putString("title", pagetitle);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_add_fos, container, false);
            TextView textView = (TextView) rootView.findViewById(R.id.section_label);
            LinearLayout linear_personal= (LinearLayout) rootView.findViewById(R.id.linear_personal_det);
            LinearLayout linear_contact_det= (LinearLayout) rootView.findViewById(R.id.linear_contact_det);
            LinearLayout linear_store_det= (LinearLayout) rootView.findViewById(R.id.linear_store_det);
            LinearLayout linear_kyc_det= (LinearLayout) rootView.findViewById(R.id.linear_kyc_det);
            Button btn_next=(Button) rootView.findViewById(R.id.btn_next);
            Button btn_submit=(Button) rootView.findViewById(R.id.btn_submit);

            final int sectionno=(getArguments().getInt(ARG_SECTION_NUMBER));
            textView.setText(getArguments().getString("title"));
            if(sectionno==0)
            {
                linear_personal.setVisibility(View.VISIBLE);
                linear_contact_det.setVisibility(View.GONE);
                linear_store_det.setVisibility(View.GONE);
                linear_kyc_det.setVisibility(View.GONE);
                btn_next.setVisibility(View.VISIBLE);
            }else if(sectionno==1)
            {
                linear_personal.setVisibility(View.GONE);
                linear_contact_det.setVisibility(View.VISIBLE);
                linear_store_det.setVisibility(View.GONE);
                linear_kyc_det.setVisibility(View.GONE);
                btn_next.setVisibility(View.VISIBLE);
            }else if(sectionno==2)
            {
                linear_personal.setVisibility(View.GONE);
                linear_contact_det.setVisibility(View.GONE);
                linear_store_det.setVisibility(View.VISIBLE);
                linear_kyc_det.setVisibility(View.GONE);
                btn_next.setVisibility(View.VISIBLE);
            }else if(sectionno==3)
            {
                linear_personal.setVisibility(View.GONE);
                linear_contact_det.setVisibility(View.GONE);
                linear_store_det.setVisibility(View.GONE);
                linear_kyc_det.setVisibility(View.VISIBLE);
                btn_next.setVisibility(View.GONE);
            }else{

            }
            btn_next.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    mViewPager.setCurrentItem(sectionno + 1);

                }
            });

            return rootView;
        }
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return PlaceholderFragment.newInstance(position, String.valueOf(getPageTitle(position)));
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 4;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "Personal Details";
                case 1:
                    return "Contact Details";
                case 2:
                    return "Store Details";
                case 3:
                    return "KYC Details";
            }
            return null;
        }
    }
}
