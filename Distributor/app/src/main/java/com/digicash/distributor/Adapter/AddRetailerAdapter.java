package com.digicash.distributor.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.digicash.distributor.FundRequest.FundBankDetails;
import com.digicash.distributor.Model.Item;
import com.digicash.distributor.R;

import java.util.List;

/**
 * Created by shraddha on 22-03-2018.
 */

public class AddRetailerAdapter extends RecyclerView.Adapter<AddRetailerAdapter.MyViewHolder>{

    private List<Item> mList;
    private List<Item> postList;
    private List<Item> preList;
    Context ctx;
    String frg_name="";
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txt_label1, txt_label2, txt_label3,txt_label4,txt_label5,txt_label6,txt_label7,txt_label8,txt_label9,txt_raise_cmp;
        //Typeface font ;
        public MyViewHolder(View view) {
            super(view);
            txt_label1 = (TextView) view.findViewById(R.id.txt_label1);
            txt_label2 = (TextView) view.findViewById(R.id.txt_label2);
            txt_label3 = (TextView) view.findViewById(R.id.txt_label3);
            txt_label4 = (TextView) view.findViewById(R.id.txt_label4);
            txt_label5 = (TextView) view.findViewById(R.id.txt_label5);
            txt_label8 = (TextView) view.findViewById(R.id.txt_label8);
            txt_label9 = (TextView) view.findViewById(R.id.txt_label9);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String operator_name=txt_label8.getText().toString();
                    String id=txt_label9.getText().toString();
                    Intent intent=new Intent(ctx, FundBankDetails.class);
                    intent.putExtra("operator_circle_name",operator_name);
                    intent.putExtra("operator_circle_id",id);
                    //Log.d("ryet", id + " ");
                    ((Activity) ctx).setResult(6,intent);
                    ((Activity) ctx).finish();//finishing activity
                }
            });


        }
    }

    public AddRetailerAdapter(List<Item> mList, Context ctx, String frg_name) {
        this.mList = mList;
        this.ctx = ctx;
        this.frg_name = frg_name;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.bank_detailsadapter, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Item movie = mList.get(position);

        holder.txt_label1.setText(movie.getTrans_no()+"");
        holder.txt_label2.setText(movie.getTrans_type()+"");

    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

}
