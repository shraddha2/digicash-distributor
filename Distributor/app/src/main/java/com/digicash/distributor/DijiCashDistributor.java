package com.digicash.distributor;

import android.app.Application;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.digicash.distributor.Receiver.ConnectivityReceiver;

public class DijiCashDistributor extends Application {

    private static com.digicash.distributor.DijiCashDistributor mInstance;
    private FirebaseAnalytics mFirebaseAnalytics;
    @Override
    public void onCreate() {
        super.onCreate();

        mInstance = this;
        // Obtain the FirebaseAnalytics instance.
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
    }

    public static synchronized com.digicash.distributor.DijiCashDistributor getInstance() {
        return mInstance;


    }

    public void setConnectivityListener(ConnectivityReceiver.ConnectivityReceiverListener listener) {
        ConnectivityReceiver.connectivityReceiverListener = listener;
    }


}

