package com.digicash.distributor.Activity;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.SmsManager;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.digicash.distributor.DefineData;
import com.digicash.distributor.R;

import java.util.List;

import static android.Manifest.permission.SEND_SMS;

public class OfflineModeActivity extends AppCompatActivity {

    LinearLayout l_check_balance,l_recharge,l_recharge_hist,l_get_status;
    private static final int REQUEST_SMS = 0;
    public  static final int RequestPermissionCode  = 1 ;
    private BroadcastReceiver sentStatusReceiver, deliveredStatusReceiver;
    String msg_keyword="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offline_mode);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle("Dijicash");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        l_check_balance= (LinearLayout) findViewById(R.id.l_check_balance);
        l_recharge= (LinearLayout) findViewById(R.id.l_recharge);
        l_recharge_hist= (LinearLayout) findViewById(R.id.l_recharge_hist);
        l_get_status= (LinearLayout) findViewById(R.id.l_get_status);


        l_check_balance.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(OfflineModeActivity.this);
                builder.setTitle("Confirmation");
                builder.setMessage("Are you sure you want to send sms?");
                builder.setPositiveButton("YES",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                                    int hasSMSPermission = checkSelfPermission(SEND_SMS);
                                    if (hasSMSPermission != PackageManager.PERMISSION_GRANTED) {
                                        if (!shouldShowRequestPermissionRationale(SEND_SMS)) {
                                            showMessageOKCancel("You need to allow access to Send SMS",
                                                    new DialogInterface.OnClickListener() {
                                                        @Override
                                                        public void onClick(DialogInterface dialog, int which) {
                                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                                requestPermissions(new String[] {SEND_SMS},
                                                                        REQUEST_SMS);
                                                            }
                                                        }
                                                    });
                                            return;
                                        }
                                        requestPermissions(new String[] {SEND_SMS},
                                                REQUEST_SMS);
                                        return;
                                    }
                                    msg_keyword="BAL";
                                    sendMySMS(msg_keyword);

                                }else{
                                    msg_keyword="BAL";
                                    sendMySMS(msg_keyword);
                                }

                            }
                        });
                builder.setNegativeButton("NO",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                dialog.dismiss();
                            }
                        });
                builder.show();

            }
        });

        l_recharge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showCategoryDialog();
            }
        });

        l_recharge_hist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(OfflineModeActivity.this);
                builder.setTitle("Confirmation");
                builder.setMessage("Are you sure you want to send sms?");
                builder.setPositiveButton("YES",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                                    int hasSMSPermission = checkSelfPermission(SEND_SMS);
                                    if (hasSMSPermission != PackageManager.PERMISSION_GRANTED) {
                                        if (!shouldShowRequestPermissionRationale(SEND_SMS)) {
                                            showMessageOKCancel("You need to allow access to Send SMS",
                                                    new DialogInterface.OnClickListener() {
                                                        @Override
                                                        public void onClick(DialogInterface dialog, int which) {
                                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                                requestPermissions(new String[] {SEND_SMS},
                                                                        REQUEST_SMS);
                                                            }
                                                        }
                                                    });
                                            return;
                                        }
                                        requestPermissions(new String[] {SEND_SMS},
                                                REQUEST_SMS);
                                        return;
                                    }else {
                                        msg_keyword = "RH";
                                        sendMySMS(msg_keyword);
                                    }

                                }
                            }
                        });
                builder.setNegativeButton("NO",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                dialog.dismiss();
                            }
                        });
                builder.show();



            }
        });

        l_get_status.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                final AlertDialog.Builder builder = new AlertDialog.Builder(OfflineModeActivity.this);
                builder.setTitle("Confirmation");
                builder.setMessage("Are you sure you want to send sms?");
                builder.setPositiveButton("YES",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                                    int hasSMSPermission = checkSelfPermission(SEND_SMS);
                                    if (hasSMSPermission != PackageManager.PERMISSION_GRANTED) {
                                        if (!shouldShowRequestPermissionRationale(SEND_SMS)) {
                                            showMessageOKCancel("You need to allow access to Send SMS",
                                                    new DialogInterface.OnClickListener() {
                                                        @Override
                                                        public void onClick(DialogInterface dialog, int which) {
                                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                                requestPermissions(new String[] {SEND_SMS},
                                                                        REQUEST_SMS);
                                                            }
                                                        }
                                                    });
                                            return;
                                        }
                                        requestPermissions(new String[] {SEND_SMS},
                                                REQUEST_SMS);
                                        return;
                                    }

                                    showChangeLangDialog();
                                }else{
                                    showChangeLangDialog();
                                }

                            }
                        });
                builder.setNegativeButton("NO",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                dialog.dismiss();
                            }
                        });
                builder.show();

            }
        });


    }

    public void sendMySMS(String message ) {

        String phone = DefineData.OFFLINE_MOBILE_NUMBER;

        //Check if the phoneNumber is empty
        if (phone.isEmpty()) {
            Toast.makeText(getApplicationContext(), "Please Enter a Valid Phone Number", Toast.LENGTH_SHORT).show();
        } else {

            SmsManager sms = SmsManager.getDefault();
            // if message length is too long messages are divided
            List<String> messages = sms.divideMessage(message);
            for (String msg : messages) {

                PendingIntent sentIntent = PendingIntent.getBroadcast(this, 0, new Intent("SMS_SENT"), 0);
                PendingIntent deliveredIntent = PendingIntent.getBroadcast(this, 0, new Intent("SMS_DELIVERED"), 0);
                sms.sendTextMessage(phone, null, msg, sentIntent, deliveredIntent);

            }
        }
    }
    public void onResume() {
        super.onResume();
        sentStatusReceiver=new BroadcastReceiver() {

            @Override
            public void onReceive(Context arg0, Intent arg1) {
                String s = "Unknown Error";
                switch (getResultCode()) {
                    case Activity.RESULT_OK:
                        s = "Message Sent Successfully !!";
                        break;
                    case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
                        s = "Generic Failure Error";
                        break;
                    case SmsManager.RESULT_ERROR_NO_SERVICE:
                        s = "Error : No Service Available";
                        break;
                    case SmsManager.RESULT_ERROR_NULL_PDU:
                        s = "Error : Null PDU";
                        break;
                    case SmsManager.RESULT_ERROR_RADIO_OFF:
                        s = "Error : Radio is off";
                        break;
                    default:
                        break;
                }
                Toast.makeText(getApplicationContext(),s+"", Toast.LENGTH_LONG).show();

            }
        };
        deliveredStatusReceiver=new BroadcastReceiver() {

            @Override
            public void onReceive(Context arg0, Intent arg1) {
                String s = "Message Not Delivered";
                switch(getResultCode()) {
                    case Activity.RESULT_OK:
                        s = "Message Delivered Successfully";
                        break;
                    case Activity.RESULT_CANCELED:
                        break;
                }
                Toast.makeText(getApplicationContext(),s+"", Toast.LENGTH_LONG).show();

            }
        };
        registerReceiver(sentStatusReceiver, new IntentFilter("SMS_SENT"));
        //registerReceiver(deliveredStatusReceiver, new IntentFilter("SMS_DELIVERED"));
    }


    public void onPause() {
        super.onPause();
        unregisterReceiver(sentStatusReceiver);
        //unregisterReceiver(deliveredStatusReceiver);
    }
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_SMS:
                if (grantResults.length > 0 &&  grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    Toast.makeText(getApplicationContext(), "Permission Granted, Now you can access sms", Toast.LENGTH_SHORT).show();
                    sendMySMS(msg_keyword);

                }else {
                    Toast.makeText(getApplicationContext(), "Permission Denied, You cannot access and sms", Toast.LENGTH_SHORT).show();
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (shouldShowRequestPermissionRationale(SEND_SMS)) {
                            showMessageOKCancel("You need to allow access to both the permissions",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                requestPermissions(new String[]{SEND_SMS},
                                                        REQUEST_SMS);
                                            }
                                        }
                                    });
                            return;
                        }
                    }
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new android.support.v7.app.AlertDialog.Builder(OfflineModeActivity.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        /// unregisterReceiver(sentStatusReceiver);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    public void showChangeLangDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.cust_edit_dialog, null);
        dialogBuilder.setView(dialogView);

        final EditText edt = (EditText) dialogView.findViewById(R.id.edit1);


        dialogBuilder.setTitle("Get Recharge Status");
        dialogBuilder.setMessage("Enter Mobile Number");
        dialogBuilder.setPositiveButton("Send", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //do something with edt.getText().toString();
                dialog.dismiss();
                String mb_number=edt.getText().toString();
                msg_keyword="SN "+mb_number;
                sendMySMS(msg_keyword);
            }
        });
        dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //pass
                dialog.dismiss();
            }
        });
        AlertDialog b = dialogBuilder.create();
        b.show();
    }

    public void showCategoryDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.cust_category_layout, null);
        dialogBuilder.setView(dialogView);
        dialogBuilder.setTitle("Select Recharge Type");
        final LinearLayout linear_prepaid = (LinearLayout) dialogView.findViewById(R.id.linear_prepaid);
        final LinearLayout linear_postpaid = (LinearLayout) dialogView.findViewById(R.id.linear_postpaid);
        final LinearLayout linear_dth = (LinearLayout) dialogView.findViewById(R.id.linear_dth);

        linear_prepaid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(OfflineModeActivity.this,OperatorListActivity.class);
                i.putExtra("type",1);
                startActivity(i);
                finish();
            }
        });

        linear_postpaid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(OfflineModeActivity.this,OperatorListActivity.class);
                i.putExtra("type",2);
                startActivity(i);
                finish();
            }
        });

        linear_dth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(OfflineModeActivity.this,OperatorListActivity.class);
                i.putExtra("type",3);
                startActivity(i);
                finish();
            }
        });

        dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                dialog.dismiss();
            }
        });
        AlertDialog b = dialogBuilder.create();
        b.show();
    }
}
