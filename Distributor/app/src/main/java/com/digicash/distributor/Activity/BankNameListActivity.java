package com.digicash.distributor.Activity;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.digicash.distributor.Adapter.OperatorCircleAdapter;
import com.digicash.distributor.Model.DividerItemDecoration;
import com.digicash.distributor.Model.Item;
import com.digicash.distributor.R;

import java.util.ArrayList;

public class BankNameListActivity extends AppCompatActivity {
    private ArrayList<Item> movieList = new ArrayList<>();
    private RecyclerView recyclerView;
    private OperatorCircleAdapter mAdapter;
    private FirebaseAnalytics mFirebaseAnalytics;
    String source="";
    TextView txt_error, txt_network_msg, txt_err_msgg;
    LinearLayout progress_linear,linear_container;
    RelativeLayout rel_no_records,rel_no_internet;
    Button btn_refresh;
    SwipeRefreshLayout mSwipeRefreshLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_operator_circle);

        // Obtain the FirebaseAnalytics instance.
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle("Select Bank");
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        txt_error= (TextView) findViewById(R.id.txt_error);
        recyclerView = (RecyclerView) findViewById(R.id.rc_retailers);

        txt_err_msgg= (TextView) findViewById(R.id.txt_err_msgg);
        txt_network_msg= (TextView) findViewById(R.id.txt_network_msg);

        progress_linear= (LinearLayout) findViewById(R.id.loding);
        linear_container= (LinearLayout) findViewById(R.id.container);
        rel_no_records= (RelativeLayout) findViewById(R.id.rel_no_records);
        rel_no_internet= (RelativeLayout) findViewById(R.id.rel_no_internet);
        btn_refresh= (Button) findViewById(R.id.btn_refresh);
        mSwipeRefreshLayout= (SwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout);

        source=getIntent().getStringExtra("source");

        movieList = fill_with_data2();
        mAdapter = new OperatorCircleAdapter(movieList,this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new DividerItemDecoration(BankNameListActivity.this));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

        progress_linear.setVisibility(View.GONE);
        linear_container.setVisibility(View.VISIBLE);
        rel_no_records.setVisibility(View.GONE);
        rel_no_internet.setVisibility(View.GONE);
        txt_network_msg.setVisibility(View.GONE);
        txt_err_msgg.setVisibility(View.GONE);

    }

    public ArrayList<Item> fill_with_data2() {

        ArrayList<Item> list_banks = new ArrayList<>();
        //   list_banks.add(0,new Item("--Select--","0"));
        list_banks.add(new Item("Abhyudaya Co-Op Bank","ABHY"));
        list_banks.add(new Item("ABN Amro Bank Credit Card","ABCC"));
        list_banks.add(new Item("Abu Dhabi Commercial Bank","ADCB"));
        list_banks.add(new Item("Adarsh Urban Co-Op Bank, Hyderabad","ICIA"));
        list_banks.add(new Item("Ahmedabad District Central Co-Op Bank","GSAD"));
        list_banks.add(new Item("Ahmedabad Mercantile Co-Op Bank","AMCB"));
        list_banks.add(new Item("Ahmednagar Mer Co-Op Bank","AGBL"));
        list_banks.add(new Item("Aircel Smart Money Ltd","ASML"));
        list_banks.add(new Item("Airtel Payments Bank","AIRP"));
        list_banks.add(new Item("Akhand Anand Co-Op Bank","AACB"));
        list_banks.add(new Item("Akola District Central Co-operative Bank","ADCC"));
        list_banks.add(new Item("Akola Janata Commercial Co Operative Bank Ltd","AKJB"));
        list_banks.add(new Item("Allahabad Bank","ALLA"));
        list_banks.add(new Item("Allahabad UP Gramin Bank","ALLG"));
        list_banks.add(new Item("Ambarnath Jai Hind Co-Op Bank","AJHB"));
        list_banks.add(new Item("American Express Credit Card","AMEX"));
        list_banks.add(new Item("Andhra Bank","ANDB"));
        list_banks.add(new Item("Andhra Bank Credit Card","ANCC"));
        list_banks.add(new Item("Andhra Pradesh Grameena Vikas Bank","SAPG"));
        list_banks.add(new Item("Andhra Pragathi Grameena Bank","APGB"));
        list_banks.add(new Item("AP Mahesh Co-Op Urban Bank","APMC"));
        list_banks.add(new Item("Apna Sahakari Bank","ASBL"));
        list_banks.add(new Item("Arunachal Pradesh Rural Bank","SBAP"));
        list_banks.add(new Item("Aryavart Gramin Bank","BKIG"));
        list_banks.add(new Item("Assam Gramin Vikash Bank","UASG"));
        list_banks.add(new Item("Axis Bank","UTIB"));
        list_banks.add(new Item("Baitarani Gramin Bank","BKIB"));
        list_banks.add(new Item("Ballia Etawah Gramin Bank","CBIG"));
        list_banks.add(new Item("Bandhan Bank","BDBL"));
        list_banks.add(new Item("Bangiya Gramin Bank","UTBB"));
        list_banks.add(new Item("Bank of America","BOFA"));
        list_banks.add(new Item("Bank of Bahrain and Kuwait","BBKM"));
        list_banks.add(new Item("Bank of Baroda","BARB"));
        list_banks.add(new Item("Bank Of Baroda Credit Card","BABC"));
        list_banks.add(new Item("Bank of Ceylon","BCEY"));
        list_banks.add(new Item("Bank of India","BKID"));
        list_banks.add(new Item("Bank Of India Credit Card","BKCC"));
        list_banks.add(new Item("Bank of Maharashtra","MAHB"));
        list_banks.add(new Item("Bank of Nova Scotia","NOSC"));
        list_banks.add(new Item("Bank of Tokyo-Mitsubishi UFJ","BOTM"));
        list_banks.add(new Item("Barclays Bank","BARC"));
        list_banks.add(new Item("Barclays Credit Card","BACC"));
        list_banks.add(new Item("Baroda Gujarat Gramin Bank","BGGB"));
        list_banks.add(new Item("Baroda Rajasthan Kshetriya Gramin Bank","BARR"));
        list_banks.add(new Item("Baroda Uttar Pradesh Gramin Bank","BARU"));
        list_banks.add(new Item("Bassein Catholic Co-Op Bank","BACB"));
        list_banks.add(new Item("Bharat Co-Op Bank, Mumbai","BCBM"));
        list_banks.add(new Item("Bhartiya Mahila Bank","BMBL"));
        list_banks.add(new Item("Bihar Kshetriya Gramin Bank","UCBK"));
        list_banks.add(new Item("BNP Paribas","BNPA"));
        list_banks.add(new Item("Bombay Mercantile Co-Op Bank","BMCL"));
        list_banks.add(new Item("Canara Bank","CNRB"));
        list_banks.add(new Item("Canara Bank Credit Card","CBCC"));
        list_banks.add(new Item("Catholic Syrian Bank","CSBK"));
        list_banks.add(new Item("Central Bank of India","CBIN"));
        list_banks.add(new Item("Chaitanya Godavari Grameena Bank","ACGG"));
        list_banks.add(new Item("Chhattisgarh Gramin Bank","SBIC"));
        list_banks.add(new Item("Chickmangalur Kodagu Gramin Bank","CORG"));
        list_banks.add(new Item("Chikhli Urban Co-Op Bank","CUCB"));
        list_banks.add(new Item("Chinatrust Commercial Bank","CTCB"));
        list_banks.add(new Item("Citibank","CITI"));
        list_banks.add(new Item("Citibank Credit Card","CICC"));
        list_banks.add(new Item("Citizen Co-Op Bank, Noida","COBL"));
        list_banks.add(new Item("Citizen Credit Co-Op Bank","CCBL"));
        list_banks.add(new Item("City Union Bank","CIUB"));
        list_banks.add(new Item("Corporation Bank","CORP"));
        list_banks.add(new Item("Cosmos Co-Op Bank","COSB"));
        list_banks.add(new Item("Credit Agricole Corp N Investment Bank","CRLY"));
        list_banks.add(new Item("Dapoli Urban Co-Op Bank","IBDU"));
        list_banks.add(new Item("DBS Bank","DBSS"));
        list_banks.add(new Item("Delhi State Co-Op Bank","DSCB"));
        list_banks.add(new Item("Dena Bank","BKDN"));
        list_banks.add(new Item("Dena Gujarat Gramin Bank","BKDD"));
        list_banks.add(new Item("Deutsche Bank AG","DEUT"));
        list_banks.add(new Item("Development Credit Bank","DCBL"));
        list_banks.add(new Item("Dhanlaxmi Bank","DLXB"));
        list_banks.add(new Item("DICGC","DICG"));
        list_banks.add(new Item("Dombivli East","SBDO"));
        list_banks.add(new Item("Dombivli Nagari Sahakari Bank","DNSB"));
        list_banks.add(new Item("Dr. Annasaheb Chougule Urban Co-Op Bank","HDFA"));
        list_banks.add(new Item("Durg Rajnandgaon Gramin Bank","BKDR"));
        list_banks.add(new Item("Ellaqui Dehati Bank","SBIE"));
        list_banks.add(new Item("Equitas Small Finance Bank","ESFB"));
        list_banks.add(new Item("ESAF Small Finance Bank","ESMF"));
        list_banks.add(new Item("Firstrand Bank","FIRN"));
        list_banks.add(new Item("Gayatri Bank","HDGB"));
        list_banks.add(new Item("Greater Bombay Co-Op Bank","GBCB"));
        list_banks.add(new Item("Gujarat State Co-Op Bank","GSCB"));
        list_banks.add(new Item("Gurgaon Gramin Bank","GGBK"));
        list_banks.add(new Item("Hadoti Kshetriya Gramin Bank","CBIH"));
        list_banks.add(new Item("Hamirpur District Co-Op Bank, Mahoba","ICMA"));
        list_banks.add(new Item("Hasti Co-Op Bank","HCBL"));
        list_banks.add(new Item("HDFC Bank","HDFC"));
        list_banks.add(new Item("HDFC Bank Credit Card","HDCC"));
        list_banks.add(new Item("Himachal Gramin Bank","PUHG"));
        list_banks.add(new Item("Himachal Pradesh State Co-Op Bank","HPSC"));
        list_banks.add(new Item("HSBC","HSBC"));
        list_banks.add(new Item("HSBC Credit Card","HSCC"));
        list_banks.add(new Item("Hutatma Sahakari Bank","ICIH"));
        list_banks.add(new Item("ICICI Bank","ICIC"));
        list_banks.add(new Item("ICICI Bank Credit Card","ICCC"));
        list_banks.add(new Item("IDBI Bank","IBKL"));
        list_banks.add(new Item("IDBI Bank Credit Card","IDCC"));
        list_banks.add(new Item("IDFC Bank","IDFB"));
        list_banks.add(new Item("India Post Payments Bank","IIPB"));
        list_banks.add(new Item("Indian Bank","IDIB"));
        list_banks.add(new Item("Indian Overseas Bank","IOBA"));
        list_banks.add(new Item("IndusInd Bank","INDB"));
        list_banks.add(new Item("IndusInd Bank Credit Card","IBCC"));
        list_banks.add(new Item("ING Vysya Bank","VYSA"));
        list_banks.add(new Item("Integral Urban Co-Op Bank","IUCB"));
        list_banks.add(new Item("Irinjalakuda Town Co-Op Bank","IRTO"));
        list_banks.add(new Item("J&K Grameen Bank","JAKG"));
        list_banks.add(new Item("Jaipur Thar Gramin Bank","UJTG"));
        list_banks.add(new Item("Jalaun District Co-Op Bank","JDCB"));
        list_banks.add(new Item("Jalgaon Peoples Co-Op Bank","JPCB"));
        list_banks.add(new Item("Jalore Nagrik Sahakari Bank","HDJC"));
        list_banks.add(new Item("Jamia Co-Op Bank","JCBL"));
        list_banks.add(new Item("Jammu and Kashmir Bank","JAKA"));
        list_banks.add(new Item("Janakalyan Sahakari Bank","JSBL"));
        list_banks.add(new Item("Janaseva Sahakari Bank","JANA"));
        list_banks.add(new Item("Janata Co-Op Bank, Malegaon","HDFJ"));
        list_banks.add(new Item("Janata Sahakari Bank,Pune","JSBP"));
        list_banks.add(new Item("Jhabua Dhar Kshetriya Gramin Bank","BJDG"));
        list_banks.add(new Item("Jharkhand Gramin Bank","SBIJ"));
        list_banks.add(new Item("JP Morgan Chase Bank","CHAS"));
        list_banks.add(new Item("Kalinga Gramya Bank","UCKG"));
        list_banks.add(new Item("Kallapana Ichalkaranji Awade Janaseva Sahakari Bank","KAIJ"));
        list_banks.add(new Item("Kalupur Commercial Co-Op Bank","KCCB"));
        list_banks.add(new Item("Kalyan Janata Sahakari Bank","KJSB"));
        list_banks.add(new Item("Kangra Central Co-Op Bank","KACE"));
        list_banks.add(new Item("Kangra Co-Op Bank","KANG"));
        list_banks.add(new Item("Kapole Co-Op Bank","KCBL"));
        list_banks.add(new Item("Karad Urban Co-Op Bank","KUCB"));
        list_banks.add(new Item("Karnataka Bank","KARB"));
        list_banks.add(new Item("Karnataka State Apex Co-Op","KSBC"));
        list_banks.add(new Item("Karnataka State Co-Op Apex Bank, Bangalore","KSCB"));
        list_banks.add(new Item("Karnataka Vikas Grameena Bank","KVGB"));
        list_banks.add(new Item("Karur Vysya Bank","KVBL"));
        list_banks.add(new Item("Kashi Gomati Samyut Gramin Bank","UBKG"));
        list_banks.add(new Item("Kaveri Grameena Bank","SBMG"));
        list_banks.add(new Item("Kerala Gramin Bank","KLGB"));
        list_banks.add(new Item("Kotak Mahindra Bank","KKBK"));
        list_banks.add(new Item("Kotak Mahindra Credit Card","KKCC"));
        list_banks.add(new Item("Krishna Gramin Bank","SKRG"));
        list_banks.add(new Item("Kurmanchal Nagar Sahkari Bank Ltd","KNSB"));
        list_banks.add(new Item("Lakshmi Vilas Bank","LAVB"));
        list_banks.add(new Item("Langpi Dehangi Rural Bank","SLDR"));
        list_banks.add(new Item("Madhya Bharat Gramin Bank","FBIG"));
        list_banks.add(new Item("Madhya Bihar Gramin Bank","PUNG"));
        list_banks.add(new Item("Madhyanchal Gramin Bank, Sagar","MGBS"));
        list_banks.add(new Item("Mahakaushal Kshetriya Gramin Bank","UCBG"));
        list_banks.add(new Item("Mahanagar Co-Op Bank","MCBL"));
        list_banks.add(new Item("Maharashtra Gramin Bank","MAHG"));
        list_banks.add(new Item("Maharashtra State Co-Op Bank","MSCI"));
        list_banks.add(new Item("Malad Sahkari Bank","MASB"));
        list_banks.add(new Item("Malda District Central Co-Op Bank","MDCB"));
        list_banks.add(new Item("Malwa Gramin Bank","HDFG"));
        list_banks.add(new Item("Manipur Rural ANK","UTBG"));
        list_banks.add(new Item("Manvi Pattana Souharda Sahakari Bank","MPSS"));
        list_banks.add(new Item("Maratha Co-Op Bank","MCOB"));
        list_banks.add(new Item("Mashreq Bank PSC","MSHQ"));
        list_banks.add(new Item("Mayani Urban Co-Op Bank","ICIM"));
        list_banks.add(new Item("Meghalaya Rural Bank","SMEG"));
        list_banks.add(new Item("Mehsana Urban Co-Op Bank","MSNU"));
        list_banks.add(new Item("Mewar Anchalik Gramin Bank","ICIG"));
        list_banks.add(new Item("MG Baroda Gramin Bank","SBBG"));
        list_banks.add(new Item("MGCB Main","WBMG"));
        list_banks.add(new Item("Mizoram Rural Bank","SBIG"));
        list_banks.add(new Item("Mizuho Corporate Bank","MHCB"));
        list_banks.add(new Item("Mogaveera Co-Op Bank","MGCB"));
        list_banks.add(new Item("Moradabad Zila Sahkari Bank","MZSB"));
        list_banks.add(new Item("Mumbai District Central Co-Operative Bank Ltd","DCCL"));
        list_banks.add(new Item("Municipal Co-Op Bank","MUBL"));
        list_banks.add(new Item("Nagar Sahkari Bank","NASB"));
        list_banks.add(new Item("Nainital Almora Kshetriya Gramin Bank","BARG"));
        list_banks.add(new Item("Nainital Bank","NTBL"));
        list_banks.add(new Item("Nashik Merchants Co-Op Bank","NMCB"));
        list_banks.add(new Item("National Co-Op Bank","KKBN"));
        list_banks.add(new Item("Neelachal Gramya Bank","INGB"));
        list_banks.add(new Item("NEFT Malwa Gramin Bank","NMGB"));
        list_banks.add(new Item("New India Co-Op Bank","NICB"));
        list_banks.add(new Item("NKGSB Co-Op Bank","NKGS"));
        list_banks.add(new Item("Noble Co-Op Bank","NCBL"));
        list_banks.add(new Item("North Malabar Gramin Bank","SYNM"));
        list_banks.add(new Item("Nutan Nagarik Sahakari Bank","NNSB"));
        list_banks.add(new Item("Odisha Gramya Bank","IOGB"));
        list_banks.add(new Item("Oman International Bank Saog","OIBA"));
        list_banks.add(new Item("Oriental Bank of Commerce","ORBC"));
        list_banks.add(new Item("Pachora Peoples Co-Op Bank","PPCB"));
        list_banks.add(new Item("Pallavan Grama Bank","IDIG"));
        list_banks.add(new Item("Pandharpur Merchant Co-Op Bank","ICIP"));
        list_banks.add(new Item("Pandharpur Urban Co-Op Bank","ICPU"));
        list_banks.add(new Item("Pandyan Gramin Bank","IOBG"));
        list_banks.add(new Item("Parshwanath Co-Op Bank","HDPA"));
        list_banks.add(new Item("Parsik Janata Sahakari Bank","PJSB"));
        list_banks.add(new Item("Parvatiya Gramin Bank","SPGB"));
        list_banks.add(new Item("Paschim Banga Gramin Bank","UPBG"));
        list_banks.add(new Item("Pavana Sahakari Bank","PSBL"));
        list_banks.add(new Item("Pithoragarh Jila Sahkari Bank","IBJS"));
        list_banks.add(new Item("Pochampally Co-Op Urban Bank","HDFP"));
        list_banks.add(new Item("Poornawadi Nagrik Sahakari Bank","PNSB"));
        list_banks.add(new Item("Pragathi Gramin Bank","CPGB"));
        list_banks.add(new Item("Pragathi Krishna Gramin Bank","PKGB"));
        list_banks.add(new Item("Prathama Bank","PRTH"));
        list_banks.add(new Item("Puduvai Bharathiar Grama Bank","IPBG"));
        list_banks.add(new Item("Pune Cantonment Sahakari Bank","PCSB"));
        list_banks.add(new Item("Pune Peoples Co-Op Bank","IBKP"));
        list_banks.add(new Item("Punjab and Maharashtra Co-Op Bank","PMCB"));
        list_banks.add(new Item("Punjab and Sind Bank","PSIB"));
        list_banks.add(new Item("Punjab Gramin Bank","PPGB"));
        list_banks.add(new Item("Punjab National Bank","PUNB"));
        list_banks.add(new Item("Punjab National Bank Credit Card","PBCC"));
        list_banks.add(new Item("Purvanchal Gramin Bank","SRGB"));
        list_banks.add(new Item("Raipur Urban Mercantile Co-Op Bank","HDRU"));
        list_banks.add(new Item("Rajapur Urban Co-Op Bank","ICRU"));
        list_banks.add(new Item("Rajasthan Gramin Bank","PRGB"));
        list_banks.add(new Item("Rajasthan Marudhara Gramin Bank","SBRM"));
        list_banks.add(new Item("Rajgurunagar Sahakari Bank","RSBL"));
        list_banks.add(new Item("Rajkot Nagarik Sahakari Bank","RNSB"));
        list_banks.add(new Item("Ratnakar Bank","RATN"));
        list_banks.add(new Item("RBL Bank Credit Card","RBCC"));
        list_banks.add(new Item("Reserve Bank of India","RBIS"));
        list_banks.add(new Item("Rewa-Sidhi Gramin Bank","URSG"));
        list_banks.add(new Item("Royal Bank of Scotland","ABNA"));
        list_banks.add(new Item("Rushikulya Gramin Bank","ANDG"));
        list_banks.add(new Item("Sadhana Sahakari Bank Ltd","SSBL"));
        list_banks.add(new Item("Samastipur Kshetriya Gramin Bank","SSKG"));
        list_banks.add(new Item("Sapthagiri Grameena Bank","SGCB"));
        list_banks.add(new Item("Saraswat Co-Op Bank","SRCB"));
        list_banks.add(new Item("Sardar Bhiladwala Pardi Peoples Co-Op Bank","SBPP"));
        list_banks.add(new Item("Sarva Haryana Gramin Bank","PUNH"));
        list_banks.add(new Item("Sarva UP Gramin Bank","PSGB"));
        list_banks.add(new Item("Satpura Narmada Kshetriya Gramin Bank","CSUG"));
        list_banks.add(new Item("Saurashtra Gramin Bank","SSGB"));
        list_banks.add(new Item("Seva Vikas Co-Op Bank","SVBL"));
        list_banks.add(new Item("Sharad Sahakari Bank Ltd. Manchar","SSBM"));
        list_banks.add(new Item("Sharda Gramin Bank","ASGB"));
        list_banks.add(new Item("Shinhan Bank","SHBK"));
        list_banks.add(new Item("Shirpur Peoples Co-Op Bank","KKBS"));
        list_banks.add(new Item("Shivajirao Bhosale Sahakari Bank","SHBH"));
        list_banks.add(new Item("Shivalik Mercantile Co-Op Bank","IBSM"));
        list_banks.add(new Item("Shree Mahalaxmi Co-Op Bank","SML"));
        list_banks.add(new Item("Shree Sharada Sahakari Bank","SSSB"));
        list_banks.add(new Item("Shree Veershaiv Co-Op Bank","CVCB"));
        list_banks.add(new Item("Shreyas Gramin Bank","CSGB"));
        list_banks.add(new Item("Shri Arihant Co-Op Bank","ICSA"));
        list_banks.add(new Item("Shri Basaveshwar Sahakari Bank Niyamit Bagalkot","ICIS"));
        list_banks.add(new Item("Shri Chhatrapati Rajarshi Shahu Urban Co-Op Bank Ltd","CRUB"));
        list_banks.add(new Item("Sindhudurg District Central Co-Op Bank","HDSI"));
        list_banks.add(new Item("Siwan Central Co-Op Bank","SCCB"));
        list_banks.add(new Item("Societe Generale","SOGE"));
        list_banks.add(new Item("South Indian Bank","SIBL"));
        list_banks.add(new Item("South Malabar Gramin Bank","CMGB"));
        list_banks.add(new Item("Standard Chartered Bank","SCBL"));
        list_banks.add(new Item("Standard Chartered Credit Card","SCCC"));
        list_banks.add(new Item("State Bank of Bikaner and Jaipur","SBBJ"));
        list_banks.add(new Item("State Bank of Hyderabad","SBHY"));
        list_banks.add(new Item("State Bank of India","SBIN"));
        list_banks.add(new Item("State Bank of India Credit Card","SBCC"));
        list_banks.add(new Item("State Bank of Mauritius","STCB"));
        list_banks.add(new Item("State Bank of Mysore","SBMY"));
        list_banks.add(new Item("State Bank of Patiala","STBP"));
        list_banks.add(new Item("State Bank of Travancore","SBTR"));
        list_banks.add(new Item("Suco Souharda Sahakari Bank","HDFS"));
        list_banks.add(new Item("Surat District Co-Op Bank","SDCB"));
        list_banks.add(new Item("Surat Peoples Co-Op Bank","SPCB"));
        list_banks.add(new Item("Surguja Kshetriya Gramin Bank","CKGB"));
        list_banks.add(new Item("Suryoday Small Finance Bank","SURY"));
        list_banks.add(new Item("Sutex Co-Op Bank","SUCO"));
        list_banks.add(new Item("Sutlej Gramin Bank","PSIG"));
        list_banks.add(new Item("Suvarnayug Sahakari Bank","SUSB"));
        list_banks.add(new Item("Swarna Bharat Trust Cyber Grameen","SBCG"));
        list_banks.add(new Item("Syndicate Bank","SYNB"));
        list_banks.add(new Item("Tamilnad Mercantile Bank","TMBL"));
        list_banks.add(new Item("Tamilnadu State Apex Co-Op Bank","TNSC"));
        list_banks.add(new Item("Telangana Grameena Bank","SBHG"));
        list_banks.add(new Item("Thane Bharat Sahakari Bank","TBSB"));
        list_banks.add(new Item("Thane Janata Sahakari Bank","TJSB"));
        list_banks.add(new Item("The Federal Bank Ltd","FDRL"));
        list_banks.add(new Item("The Hindusthan Co-operative Bank Ltd.","THCB"));
        list_banks.add(new Item("The Sabarkantha District Central Cooperative Bank Ltd","SDCC"));
        list_banks.add(new Item("The Sahebrao Deshmukh Co-operative Bank Ltd.","SAHE"));
        list_banks.add(new Item("The Saurashtra Cooperative Bank Ltd","SSCB"));
        list_banks.add(new Item("The Shamrao Vithal Co-Operative Bank","SVCB"));
        list_banks.add(new Item("The Thane District Central Co-Operative Bank Ltd","TCCB"));
        list_banks.add(new Item("The Zoroastrian Cooperative Bank Limited","ZCBL"));
        list_banks.add(new Item("Thrissur District Central Co-Op Bank","TDCB"));
        list_banks.add(new Item("Titwala","SBIT"));
        list_banks.add(new Item("Tripura Gramin Bank","UTGB"));
        list_banks.add(new Item("Triveni Kshetriya Gramin Bank","TKGB"));
        list_banks.add(new Item("UBS AG","UBSW"));
        list_banks.add(new Item("UCO Bank","UCBA"));
        list_banks.add(new Item("Udaipur Mahila Samrudhi Urban Co-Op Bank","UMSC"));
        list_banks.add(new Item("Udaipur urban Co-operative bank Ltd","UUCB"));
        list_banks.add(new Item("Ujjivan Small Finance Bank","UJVN"));
        list_banks.add(new Item("Union Bank of India","UBIN"));
        list_banks.add(new Item("United Bank of India","UTBI"));
        list_banks.add(new Item("Urban Co-Op Bank","UCBL"));
        list_banks.add(new Item("UTI Axis Bank Credit Card","UTCC"));
        list_banks.add(new Item("Utkal Gramya Bank","SUUG"));
        list_banks.add(new Item("Uttar Banga Kshetriya Gramin Bank","CUKG"));
        list_banks.add(new Item("Uttar Bihar Gramin Bank","CBBB"));
        list_banks.add(new Item("Uttarakhand Gramin Bank","SUTG"));
        list_banks.add(new Item("Uttarakhand Gramin Bank","UTTB"));
        list_banks.add(new Item("Vaidyanath Urban Co-Op Bank","VUCB"));
        list_banks.add(new Item("Vananchal Gramin Bank","SVAG"));
        list_banks.add(new Item("Varachha Co-Op Bank","VARA"));
        list_banks.add(new Item("Vasai Vikas Co-op Bank","VVSB"));
        list_banks.add(new Item("Vidharbha Kshetriya Gramin Bank","CVAG"));
        list_banks.add(new Item("Vidisha Bhopal Kshetriya Gramin Bank","SBOG"));
        list_banks.add(new Item("Vijaya Bank","VIJB"));
        list_banks.add(new Item("Vijaya Credit Card","VICC"));
        list_banks.add(new Item("Vikas Souharda Co-operative Bank Limited","VSCB"));
        list_banks.add(new Item("Vishweshwar Co-Op Bank","VSBL"));
        list_banks.add(new Item("Visveshwaraya Gramin Bank","VIJG"));
        list_banks.add(new Item("Wainganga Krishna Gramin Bank","BWKG"));
        list_banks.add(new Item("West Bengal State Co-Op Bank","WBSC"));
        list_banks.add(new Item("Yadagiri Lakshmi Narasimha Swamy Co-Op Urban Bank","YESP"));
        list_banks.add(new Item("Yes Bank","YESB"));
        list_banks.add(new Item("Zila Sahkari Bank","ICZS"));
        return list_banks;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_main, menu);

        Drawable drawable = menu.findItem(R.id.search).getIcon();
        drawable = DrawableCompat.wrap(drawable);
        DrawableCompat.setTint(drawable, ContextCompat.getColor(this,android.R.color.white));

        MenuItem search = menu.findItem(R.id.search);

        SearchView searchView = (SearchView) MenuItemCompat.getActionView(search);
        search(searchView);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void search(SearchView searchView) {

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                mAdapter.getFilter().filter(newText);
                return true;
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        Intent intent=new Intent();
        setResult(5,intent);
        finish();//finishing activity

    }
}
