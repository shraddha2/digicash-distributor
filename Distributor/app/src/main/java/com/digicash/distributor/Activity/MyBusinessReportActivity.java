package com.digicash.distributor.Activity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.digicash.distributor.DijiCashDistributor;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.digicash.distributor.DefineData;
import com.digicash.distributor.Model.HTTPURLConnection;
import com.digicash.distributor.R;
import com.digicash.distributor.Receiver.ConnectivityReceiver;

import org.json.JSONException;
import org.json.JSONObject;

public class MyBusinessReportActivity extends AppCompatActivity implements ConnectivityReceiver.ConnectivityReceiverListener {
    TextView txt_bal,txt_ret_no,txt_rech_total_bal,txt_rech_total_commission,txt_mt_total_bal,txt_mt_total_commission,txt_bp_total_bal,txt_bp_total_comm;
    String token;
    SharedPreferences sharedpreferences;
    TextView txt_err_msgg,txt_network_msg;
    LinearLayout progress_linear,linear_container;
    RelativeLayout rel_no_records,rel_no_internet;
    private FirebaseAnalytics mFirebaseAnalytics;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_business_report);

        // Obtain the FirebaseAnalytics instance.
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle("Report");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        txt_err_msgg= (TextView) findViewById(R.id.txt_err_msgg);
        txt_network_msg= (TextView) findViewById(R.id.txt_network_msg);

        progress_linear= (LinearLayout) findViewById(R.id.loding);
        linear_container= (LinearLayout) findViewById(R.id.container);
        rel_no_records= (RelativeLayout) findViewById(R.id.rel_no_records);
        rel_no_internet= (RelativeLayout) findViewById(R.id.rel_no_internet);

        txt_bal=(TextView)findViewById(R.id.txt_bal);
        txt_ret_no=(TextView)findViewById(R.id.txt_ret_no);
        txt_rech_total_bal=(TextView)findViewById(R.id.txt_rech_total_bal);
        txt_rech_total_commission=(TextView)findViewById(R.id.txt_rech_total_commission);
        txt_mt_total_bal=(TextView)findViewById(R.id.txt_mt_total_bal);
        txt_mt_total_commission=(TextView)findViewById(R.id.txt_mt_total_commission);
        txt_bp_total_bal=(TextView)findViewById(R.id.txt_bp_total_bal);
        txt_bp_total_comm=(TextView)findViewById(R.id.txt_bp_total_comm);
        sharedpreferences = getSharedPreferences(DefineData.LOGIN_MINKSPAY_PREFERENCE, Context.MODE_PRIVATE);
        token=sharedpreferences.getString(DefineData.TOKEN_KEY,"");

        if(checkConnection()) {
            linear_container.setVisibility(View.GONE);
            rel_no_records.setVisibility(View.GONE);
            progress_linear.setVisibility(View.GONE);
            rel_no_internet.setVisibility(View.GONE);
            new FetchData().execute();
        }else{
            linear_container.setVisibility(View.GONE);
            rel_no_records.setVisibility(View.GONE);
            progress_linear.setVisibility(View.GONE);
            rel_no_internet.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
               finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
    private class FetchData extends AsyncTask<Void, Void, Void> {
        JSONObject response;

        @Override
        protected void onPreExecute() {
            linear_container.setVisibility(View.GONE);
            rel_no_records.setVisibility(View.GONE);
            progress_linear.setVisibility(View.VISIBLE);

        }

        @Override
        protected Void doInBackground(Void... params) {
            HTTPURLConnection service = new HTTPURLConnection();
            try {
                this.response = new JSONObject(service.POST(DefineData.FETCH_BUSINESS_REPORT, token));
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            if (response != null) {
                try {
                    if (response.getBoolean("error")) {
                        txt_err_msgg.setText("oops ! Server Error");
                        linear_container.setVisibility(View.GONE);
                        rel_no_records.setVisibility(View.VISIBLE);
                        progress_linear.setVisibility(View.GONE);
                    } else {

                        JSONObject json_data=response.getJSONObject("return_data");

                        JSONObject json_dist_bal = json_data.getJSONObject("distributor_balance");
                        Double bal= json_dist_bal.getDouble("balance");

                        JSONObject json_ret = json_data.getJSONObject("retailers");
                        String no_ret= json_ret.getString("total");

                        JSONObject json_rech = json_data.getJSONObject("recharges");
                        Double totalBusiness_ret= json_rech.getDouble("totalBusiness");
                        Double totalComm_ret= json_rech.getDouble("totalComm");

                        JSONObject json_mt = json_data.getJSONObject("money_transfer");
                        Double totalBusiness_mt= json_mt.getDouble("totalBusiness");
                        Double totalComm_mt= json_mt.getDouble("totalComm");

                        JSONObject json_bp = json_data.getJSONObject("bill_payment");
                        Double totalBusiness_bp= json_bp.getDouble("totalBusiness");
                        Double totalComm_bp= json_bp.getDouble("totalComm");


                        txt_bal.setText("₹ "+String.format( "%.2f", bal)+"");
                        txt_ret_no.setText(no_ret+"");

                       txt_rech_total_bal.setText("₹ "+String.format( "%.2f", totalBusiness_ret)+"");
                        txt_rech_total_commission.setText("₹ "+String.format( "%.2f", totalComm_ret)+"");

                        txt_mt_total_bal.setText("₹ "+String.format( "%.2f", totalBusiness_mt)+"");
                        txt_mt_total_commission.setText("₹ "+String.format( "%.2f", totalComm_mt)+"");

                        txt_bp_total_bal.setText("₹ "+String.format( "%.2f", totalBusiness_bp)+"");
                        txt_bp_total_comm.setText("₹ "+String.format( "%.2f", totalComm_bp)+"");



                        linear_container.setVisibility(View.VISIBLE);
                        rel_no_records.setVisibility(View.GONE);
                        progress_linear.setVisibility(View.GONE);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();

                    txt_err_msgg.setText("oops ! Server Error");
                    linear_container.setVisibility(View.GONE);
                    rel_no_records.setVisibility(View.VISIBLE);
                    progress_linear.setVisibility(View.GONE);

                }
            } else {
                txt_err_msgg.setText("oops ! Server Error");
                linear_container.setVisibility(View.GONE);
                rel_no_records.setVisibility(View.VISIBLE);
                progress_linear.setVisibility(View.GONE);

            }
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {

    }

    @Override
    public void onResume() {
        super.onResume();
        // register connection status listener
        DijiCashDistributor.getInstance().setConnectivityListener(this);
    }

    // Method to manually check connection status
    private boolean checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        return isConnected;
    }
}
