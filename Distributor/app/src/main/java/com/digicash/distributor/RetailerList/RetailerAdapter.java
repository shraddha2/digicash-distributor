package com.digicash.distributor.RetailerList;

import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.digicash.distributor.DefineData;
import com.digicash.distributor.Model.Item;
import com.digicash.distributor.R;

import java.util.ArrayList;
import java.util.List;

public class RetailerAdapter extends RecyclerView.Adapter<RetailerAdapter.MyViewHolder> implements Filterable {

        private List<Item> moviesList;
        private List<Item> mFilteredList;
        Context ctx;
        String frg_name="";
        String token="";
        SharedPreferences sharedpreferences;

    public RetailerAdapter(List<Item> moviesList, Context ctx, String frg_name) {
            this.moviesList = moviesList;
            this.mFilteredList = moviesList;
            this.ctx = ctx;
            this.frg_name = frg_name;
            sharedpreferences = ctx.getSharedPreferences(DefineData.LOGIN_MINKSPAY_PREFERENCE, Context.MODE_PRIVATE);
            token= sharedpreferences.getString(DefineData.TOKEN_KEY,"");
        }

        @Override
        public RetailerAdapter.MyViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View view = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.retailer_row, viewGroup, false);
            return new MyViewHolder(view);
        }

        @Override
        public void onBindViewHolder(RetailerAdapter.MyViewHolder viewHolder, int i) {
            viewHolder.txt_label1.setText(mFilteredList.get(i).getTrans_no()+"");
            viewHolder.txtBusinessName.setText(mFilteredList.get(i).getTrans_type());
            viewHolder.txt_label2.setText(mFilteredList.get(i).getTrans_amount()+"");
            viewHolder.txt_label3.setText(mFilteredList.get(i).getTrans_datetime());
            viewHolder.txt_label4.setText(mFilteredList.get(i).getTrans_update_balance()+"");
            viewHolder.txt_label5.setText("₹ "+mFilteredList.get(i).getStatus()+"");
            viewHolder.txtLastFundsTaken.setText(mFilteredList.get(i).getP_name()+"");
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            public TextView txt_label1, txt_label2, txt_label3,txt_label4,txt_label5,txt_add_balance,txtBusinessName,txtLastFundsTaken;

            //Typeface font ;
            public MyViewHolder(View view) {
                super(view);
                txt_label1 = (TextView) view.findViewById(R.id.txt_label1);
                txtBusinessName = (TextView) view.findViewById(R.id.txtBusinessName);
                txt_label2 = (TextView) view.findViewById(R.id.txt_label2);
                txt_label3 = (TextView) view.findViewById(R.id.txt_label3);
                txt_label4 = (TextView) view.findViewById(R.id.txt_label4);
                txt_label5 = (TextView) view.findViewById(R.id.txt_label5);
                txt_add_balance = (TextView) view.findViewById(R.id.txt_add_balance);
                txtLastFundsTaken = (TextView) view.findViewById(R.id.txtLastFundsTaken);

                txt_add_balance.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String id=txt_label1.getText().toString();
                        String name = txtBusinessName.getText().toString();
                        String balance = txt_label5.getText().toString();
                        Bundle bundle=new Bundle();
                        bundle.putString("id",id);
                        bundle.putString("name",name);
                        bundle.putString("balance",balance);
                        Fragment frg=new AddBalanceFragment();
                        frg.setArguments(bundle);
                        ((FragmentActivity) ctx).getFragmentManager().beginTransaction()
                                .replace(R.id.frg_replace, frg)
                                .addToBackStack(null)
                                .commit();
                    }
                });

            }
        }

        @Override
        public int getItemCount() {
            return mFilteredList.size();
        }

        @Override
        public Filter getFilter() {

            return new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence charSequence) {

                    String charString = charSequence.toString();

                    if (charString.isEmpty()) {

                        mFilteredList = moviesList;
                    } else {

                        ArrayList<Item> filteredList = new ArrayList<>();

                        for (Item androidVersion : moviesList) {

                            if (androidVersion.getTrans_type().toLowerCase().contains(charString) || androidVersion.getTrans_update_balance().toLowerCase().contains(charString) ) {

                                filteredList.add(androidVersion);
                            }
                        }

                        mFilteredList = filteredList;
                    }

                    FilterResults filterResults = new FilterResults();
                    filterResults.values = mFilteredList;
                    return filterResults;
                }

                @Override
                protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                    mFilteredList = (ArrayList<Item>) filterResults.values;
                    notifyDataSetChanged();
                }
            };
        }
}
